package gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels;

import gtc.AL.Inspector.Panels.OSIRIS.Mimic.ComponentsOsirisMimicId;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.GUIConstants;
import gtc.DSL.GUI_Kit.ScalarMonitors.ScalarMonitorCapsule;

import java.awt.GridLayout;

import javax.swing.JPanel;

public class TunnableFilterTelemetryPanel extends JPanel {
	private ScalarMonitorCapsule  positionXCapulse_;
	private ScalarMonitorCapsule  positionYCapulse_;
	private ScalarMonitorCapsule  offsetZCapulse_;
	private ScalarMonitorCapsule  DisplacementZCapulse_;
	private ScalarMonitorCapsule  temperatureCapsule_;
	private ScalarMonitorCapsule  humidityCapsule_;

	
	public TunnableFilterTelemetryPanel(String component)
	{
		//setBackground(BasicsGUIDefaults.TRANSPARENT);
		setOpaque(false);
		setLayout(new GridLayout(6, 2));
		 
		positionXCapulse_ = new ScalarMonitorCapsule("X",component,"positionX");
		positionXCapulse_.setFont(GUIConstants.FONT_SMALL);
		positionYCapulse_ = new ScalarMonitorCapsule("Y",component,"positionY");
		positionYCapulse_.setFont(GUIConstants.FONT_SMALL);
		DisplacementZCapulse_ = new ScalarMonitorCapsule("Z",component,"DisplacementZ");
		DisplacementZCapulse_.setFont(GUIConstants.FONT_SMALL);
		offsetZCapulse_ = new ScalarMonitorCapsule("currentOffset",component,"currentOffset");
		offsetZCapulse_.setFont(GUIConstants.FONT_SMALL);
		temperatureCapsule_= new ScalarMonitorCapsule("Temperature",ComponentsOsirisMimicId.TFTEMPERATURA,"temperatureSensor2");
		temperatureCapsule_.setAccuracy(1);
		temperatureCapsule_.setFont(GUIConstants.FONT_SMALL);
		humidityCapsule_= new ScalarMonitorCapsule("Humidity",ComponentsOsirisMimicId.TFHUMIDITY,"humidity");
		humidityCapsule_.setAccuracy(1);
		humidityCapsule_.setFont(GUIConstants.FONT_SMALL);
		

		add(positionXCapulse_);
		add(positionYCapulse_);
		add(DisplacementZCapulse_);
		add(offsetZCapulse_);
		add(temperatureCapsule_);
		add(humidityCapsule_);
	}
}
