package gtc.AL.Inspector.Panels.OSIRIS.Mimic.Sketch;

import gtc.DSL.GUI_Kit.GUIBasics.BasicsGUIDefaults;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.RoundRectangle2D;
import java.util.Vector;

import javax.swing.JLabel;

import DATAACQUISITIONCOMPONENT.OutputMode;
import DATAACQUISITIONCOMPONENT.ReadoutMode;
import DATAACQUISITIONCOMPONENT.ShutterStatus;


public class CameraSketch extends JLabel {
	
	private Color color;
	private ReadoutMode readoutMode;
	private OutputMode outputMode;
	private ShutterStatus shutterStatus;
	private static final double GAP = 5;
	
	public CameraSketch(Color color)
	{
		this.color=color;
		
		this.outputMode=DATAACQUISITIONCOMPONENT.OutputMode.TWOCCDS_OUTPUTA;
		this.readoutMode=DATAACQUISITIONCOMPONENT.ReadoutMode.SIMPLE;
		this.shutterStatus=DATAACQUISITIONCOMPONENT.ShutterStatus.CLOSE_SHUTTER;
	}
	
	
	public void setReadoutMode(ReadoutMode readoutMode) {
		this.readoutMode = readoutMode;
	}

	public void setOutputMode(OutputMode outputMode) {
		this.outputMode = outputMode;
	}

	public void setShutterStatus(ShutterStatus shutterStatus) {
		this.shutterStatus = shutterStatus;
	}

	public void paintComponent(Graphics2D g) 
	{
		int x = (int)this.getVisibleRect().getX();
		int y = (int)this.getVisibleRect().getY();
	    int width = (int)(this.getVisibleRect().getWidth());
	    int height = (int)(this.getVisibleRect().getHeight());
	    
		Rectangle inside = new Rectangle(x, (int)(y+height*0.35), width, (int)(height*0.7));
		
		RoundRectangle2D.Double shape = new RoundRectangle2D.Double(inside.getX(),inside.getY(),inside.getWidth(),inside.getHeight(),10.5f,10.5f);
		GradientPaint gradientPaint = new GradientPaint((int)inside.getX(),(int)inside.getY(),color.darker(),(int)(inside.getX()+inside.getWidth()),(int)(inside.getY()+inside.getHeight()),color);
		g.setPaint(gradientPaint);
		g.fill(shape);
		
		paintOutputMode(g,inside);
		paintReadOutMode(g,inside);
		paintShutter(g,getVisibleRect());
	}
	

	private void paintShutter(Graphics2D g,Rectangle inside)
	{
		Font font = new Font("Courier", Font.PLAIN, 30);
		g.setFont(font);

		switch(shutterStatus.value())
		{
			case DATAACQUISITIONCOMPONENT.ShutterStatus._OPEN_SHUTTER:
				RoundRectangle2D.Double shapeShutterOpen = new RoundRectangle2D.Double(inside.getX(), 
																						inside.getY(), 
																						inside.getWidth()/2.5, 
																						inside.getHeight()*0.3,10.5,10.5);
				

				g.setColor(Color.WHITE);
				g.fill(shapeShutterOpen);
				g.setColor(Color.BLACK);
				g.drawString("OPEN", 
								(int)(inside.getWidth()/2.5-140),
								(int)(inside.getHeight()*0.3-25));

				break;
			case DATAACQUISITIONCOMPONENT.ShutterStatus._CLOSE_SHUTTER:
				RoundRectangle2D.Double shapeShutterClose = new RoundRectangle2D.Double(inside.getX()+(int)(inside.getWidth()/3.5), 
																							inside.getY(), 
																							(int)(inside.getWidth()/2.5), 
																							(int)(inside.getHeight()*0.3),10.5,10.5);

				g.setColor(Color.WHITE);
				g.fill(shapeShutterClose);
				g.setColor(Color.BLACK);
				g.drawString("CLOSED", 
								(int)(inside.getWidth()/2.5-10),
								(int)(inside.getHeight()*0.3-25));

				break;

		}
	}
	
	private void paintReadOutMode(Graphics2D g,Rectangle inside)
	{
		Vector<RoundRectangle2D.Double> visibles = new Vector<RoundRectangle2D.Double>(); 
		switch(readoutMode.value())
		{
			case DATAACQUISITIONCOMPONENT.ReadoutMode._SIMPLE:
				break;
			case DATAACQUISITIONCOMPONENT.ReadoutMode._FRAME_TRANSFER:
				visibles.add(new RoundRectangle2D.Double(inside.getX(),inside.getY()+inside.getHeight()/2.0,inside.getWidth(),inside.getHeight()/2.0,10.5f,10.5f));
				break;
			case DATAACQUISITIONCOMPONENT.ReadoutMode._CHARGE_SHUFFLING:
				for (int i=0;i<5;i++)
					visibles.add(new RoundRectangle2D.Double(inside.getX(),inside.getY()+i*(inside.getHeight())/5.0,inside.getWidth(),(inside.getHeight()-10)/5.0,10.5f,10.5f));
				break;
		}
		
		g.setColor(BasicsGUIDefaults.TRANSLUCID_DARK_GRAY);
		for (RoundRectangle2D.Double visible : visibles)
			g.fill(visible);
	}
	
	private void paintOutputMode(Graphics2D g,Rectangle inside)
	{
	
		
		RoundRectangle2D.Double channel1 = new RoundRectangle2D.Double(inside.getX()+5,inside.getY()+5,(inside.getWidth()-GAP)/4.0,inside.getHeight()-GAP,10.5f,10.5f);
		RoundRectangle2D.Double channel2 = new RoundRectangle2D.Double(inside.getX()+1*(inside.getWidth()-GAP)/4.0+5,inside.getY()+5,(inside.getWidth()-GAP)/4.0,inside.getHeight()-GAP,10.5f,10.5f);
		RoundRectangle2D.Double channel3 = new RoundRectangle2D.Double(inside.getX()+2*(inside.getWidth()-GAP)/4.0+5,inside.getY()+5,(inside.getWidth()-GAP)/4.0,inside.getHeight()-GAP,10.5f,10.5f);
		RoundRectangle2D.Double channel4 = new RoundRectangle2D.Double(inside.getX()+3*(inside.getWidth()-GAP)/4.0+5,inside.getY()+5,(inside.getWidth()-GAP)/4.0,inside.getHeight()-GAP,10.5f,10.5f);
		
		g.setColor(BasicsGUIDefaults.DARKER_GRAY);
		g.fill(channel1);
		g.fill(channel2);
		g.fill(channel3);
		g.fill(channel4);
		
		switch(outputMode.value())
		{
			case DATAACQUISITIONCOMPONENT.OutputMode._CCDONE_OUTPUTA:
				g.setColor(BasicsGUIDefaults.BLUE);
				g.fill(channel1);
				break;
			case DATAACQUISITIONCOMPONENT.OutputMode._CCDONE_OUTPUTB:
				g.setColor(BasicsGUIDefaults.BLUE);
				g.fill(channel2);
				break;
			case DATAACQUISITIONCOMPONENT.OutputMode._CCDONE_SPLIT_READOUT:
				g.setColor(BasicsGUIDefaults.BLUE);
				g.fill(channel1);
				g.fill(channel2);
				break;
			case DATAACQUISITIONCOMPONENT.OutputMode._CCDTWO_OUTPUTA:
				g.setColor(BasicsGUIDefaults.BLUE);
				g.fill(channel3);
				break;		

			case DATAACQUISITIONCOMPONENT.OutputMode._CCDTWO_OUTPUTB:
				g.setColor(BasicsGUIDefaults.BLUE);
				g.fill(channel4);
				break;
			case DATAACQUISITIONCOMPONENT.OutputMode._CCDTWO_SPLIT_READOUT:
				g.setColor(BasicsGUIDefaults.BLUE);
				g.fill(channel3);
				g.fill(channel4);
				break;
			case DATAACQUISITIONCOMPONENT.OutputMode._TWOCCDS_OUTPUTA:
				g.setColor(BasicsGUIDefaults.BLUE);
				g.fill(channel1);
				g.fill(channel3);
				break;
			case DATAACQUISITIONCOMPONENT.OutputMode._TWOCCDS_SPLIT_READOUT:
				g.setColor(BasicsGUIDefaults.BLUE);
				g.fill(channel1);
				g.fill(channel2);
				g.fill(channel3);
				g.fill(channel4);
				break;
		}
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2d =(Graphics2D)g;
		paintComponent(g2d);
	}

}

