package gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels;

import gtc.AL.Inspector.Panels.OSIRIS.Mimic.ComponentsOsirisMimicId;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.GUIConstants;
import gtc.DSL.GUI_Kit.EnumMonitor.CustomLabelsEMonitor;
import gtc.DSL.GUI_Kit.GUIBasics.Capsules.LabelCapsule;
import gtc.DSL.GUI_Kit.ScalarMonitors.ScalarMonitorCapsule;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JPanel;



public class CCDTelemetryPanel extends JPanel {
	
	private LabelCapsule readOutSpeedLabelCapsule;
	private CustomLabelsEMonitor cameraReadoutSpeedCapsule;
	private LabelCapsule cameraOutputLabelCapsule;
	private CustomLabelsEMonitor cameraOutputModeCapsule;
	private LabelCapsule binningLabelCapsule;
	private CustomLabelsEMonitor binningXModeCapsule;
	private CustomLabelsEMonitor binningYModeCapsule;
	private LabelCapsule cameraGainLabelCapsule;
	private CustomLabelsEMonitor cameraGainCapsule;  
	private LabelCapsule isApplayDarkActiveLabel;
	private CustomLabelsEMonitor isApplayDarkCapsule;  
	


	protected CustomLabelsEMonitor buildCustomLabelsEMonitor(String component, String magnitud, String [] labels)
	{
		CustomLabelsEMonitor customLabelsEMonitor  = new CustomLabelsEMonitor(component, magnitud,labels);
		customLabelsEMonitor.receiveMagnitudeChange(component,magnitud,0);
		customLabelsEMonitor.setLabelVisible(false);
		customLabelsEMonitor.setShowOnlyCurrentValue(true);
		customLabelsEMonitor.setFontSize(GUIConstants.FONT_SIZE);
		
		return  customLabelsEMonitor;
	}
	
	public CCDTelemetryPanel()
	{
		
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		setLayout(new GridBagLayout());

		
		readOutSpeedLabelCapsule = new LabelCapsule("ReadOut Speed");
		readOutSpeedLabelCapsule.setFont(GUIConstants.FONT_);
		c.gridx = 0;
		c.gridy = 0;
		c.weightx=0.1;
		add(readOutSpeedLabelCapsule,c);

		
		
		String [] readOutSpeedLabels = {"25 Khz","50 Khz","100 Khz","200 Khz","500 Khz","725 Khz","900 Khz"};
		cameraReadoutSpeedCapsule = buildCustomLabelsEMonitor(ComponentsOsirisMimicId.CAMERA,
																"currentReadoutSpeed",
																readOutSpeedLabels);
		cameraReadoutSpeedCapsule.setFont(GUIConstants.FONT_);
		
		c.gridx = 1;
		c.gridy = 0;
		c.gridwidth = 2;
		add(cameraReadoutSpeedCapsule,c);
		
		
		cameraOutputLabelCapsule = new LabelCapsule("Output Mode");
		cameraOutputLabelCapsule.setFont(GUIConstants.FONT_);
		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth =1;
		add(cameraOutputLabelCapsule,c);

		

		String [] cameraOutputModeLabels = {"CCD1-A","CCD1-B","CCD1-AB","CCD2-A","CCD2-B","CCD2-AB","CCD12-A","CCD12-AB"};
		cameraOutputModeCapsule = buildCustomLabelsEMonitor(ComponentsOsirisMimicId.CAMERA,
																"currentOutputMode",
																cameraOutputModeLabels);
		cameraOutputModeCapsule.setFont(GUIConstants.FONT_);
		c.gridx = 1;
		c.gridy = 1;
		c.gridwidth =2;
		add(cameraOutputModeCapsule,c);


		binningLabelCapsule = new LabelCapsule("Binning");
		binningLabelCapsule.setFont(GUIConstants.FONT_);
		c.gridx = 0;
		c.gridy = 2;
		c.gridwidth =1;
		add(binningLabelCapsule,c);

		
		String [] binningLabels = {"1","2"};
		binningXModeCapsule = buildCustomLabelsEMonitor(ComponentsOsirisMimicId.CAMERA,
																"isApplyBinningXActive",
																binningLabels);
		binningXModeCapsule.setFont(GUIConstants.FONT_);		
		
		
		
		c.gridx = 1;
		c.gridy = 2;
		c.gridwidth =1;
		add(binningXModeCapsule,c);

		binningYModeCapsule = buildCustomLabelsEMonitor(ComponentsOsirisMimicId.CAMERA,
															"isApplyBinningYActive",
															binningLabels);
		binningYModeCapsule.setFont(GUIConstants.FONT_);
		
		c.gridx = 2;
		c.gridy = 2;
		c.gridwidth =1;
		add(binningYModeCapsule,c);
		

		
		cameraGainLabelCapsule = new LabelCapsule("Gain");
		cameraGainLabelCapsule.setFont(GUIConstants.FONT_);
		
		c.gridx = 0;
		c.gridy = 3;
		c.gridwidth =1;
		add(cameraGainLabelCapsule,c);

		
		String [] cameraGainLabels = {"1","2","4.75","9.5"};
		
		cameraGainCapsule = buildCustomLabelsEMonitor(ComponentsOsirisMimicId.CAMERA,
														"currentGAIN",
														cameraGainLabels);
		cameraGainCapsule.setFont(GUIConstants.FONT_);		
		
		c.gridx = 1;
		c.gridy = 3;
		c.gridwidth =2;
		add(cameraGainCapsule,c);


		isApplayDarkActiveLabel = new LabelCapsule("Dark");
		isApplayDarkActiveLabel.setFont(GUIConstants.FONT_);
		
		c.gridx = 0;
		c.gridy = 4;
		c.gridwidth =1;
		add(isApplayDarkActiveLabel,c);
		
		String [] isApplayDarkLabels = { "FALSE", "TRUE" };
		isApplayDarkCapsule = buildCustomLabelsEMonitor(ComponentsOsirisMimicId.CAMERA,
														"isApplyDarkActive",
														isApplayDarkLabels);
		isApplayDarkCapsule.setFont(GUIConstants.FONT_);
		
		c.gridx = 1;
		c.gridy = 4;
		c.gridwidth =2;
		add(isApplayDarkCapsule,c);
		
		ScalarMonitorCapsule currentExposureTime = new ScalarMonitorCapsule("Exposure Time",ComponentsOsirisMimicId.DATAACQUISITIONCOMPONENT,"currentExposureTime");
		currentExposureTime.setFont(GUIConstants.FONT_);
		c.gridx = 0;
		c.gridy = 5;
		c.gridwidth =3;
		add(currentExposureTime,c);

	}


}
