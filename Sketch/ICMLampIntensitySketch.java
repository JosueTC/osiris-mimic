package gtc.AL.Inspector.Panels.OSIRIS.Mimic.Sketch;


import gtc.DSL.GUI_Kit.GUIBasics.BasicsGUIDefaults;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.awt.geom.RoundRectangle2D;

public class ICMLampIntensitySketch extends ICMLampSketch  {
	
	private double intensityLight=0;
	
	public ICMLampIntensitySketch(int width, int height) {
		super(width, height);
	}

	public void setIntensityLight(double intensityLight) {
		this.intensityLight = intensityLight;
	}
	
	public void draw(Graphics2D g, int X, int Y, int W, int H)
	{
		Graphics2D g2d =(Graphics2D)g;
		
		
		g2d.setColor(BasicsGUIDefaults.BACKGROUND);
		g2d.setStroke(new BasicStroke(10.0f));
		
		RoundRectangle2D.Double arc = new RoundRectangle2D.Double(X,Y,W,H,8,8);
		g2d.fill(arc);
		
		double factor = Math.min(W,H)*0.15;
		
		RoundRectangle2D.Double intestity = null;
		if (W>H) {
			intestity = new RoundRectangle2D.Double(X+factor,Y+factor,(W-2*factor)*(intensityLight/100.0),H-2*factor,8,8);
		} else {
			intestity = new RoundRectangle2D.Double(X+factor,Y+factor,(W-2*factor),(H-2*factor)*(intensityLight/100.0),8,8);
		}
		if (isOn) {
			g2d.setColor( BasicsGUIDefaults.ORANGE);
		} else {
			g2d.setColor( BasicsGUIDefaults.BACKGROUND);	
		}

		g2d.fill(intestity);
		
		Font font = new Font("Courier", Font.PLAIN, 30);
		String intesityLightLegent = Math.ceil(intensityLight)+"%";
		Dimension textSize = getFotSize(intesityLightLegent,font); 
		
		g2d.setColor(Color.WHITE);
		g2d.setFont(font);
		
		g2d.drawString(intesityLightLegent, 
						(int)(X+(W-textSize.getWidth())*0.5),
						(int)(Y+(H+textSize.getHeight()-10)*0.5));
		
		
	}
	

	
	private Dimension getFotSize(String label, Font font) {
		AffineTransform affinetransform = new AffineTransform();     
		FontRenderContext frc = new FontRenderContext(affinetransform,true,true);    
		int textwidth = (int)(font.getStringBounds(label, frc).getWidth());
		int textheight = (int)(font.getStringBounds(label, frc).getHeight());
		
		return new Dimension(textwidth,textheight);
	}

}
