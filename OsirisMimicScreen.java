package gtc.AL.Inspector.Panels.OSIRIS.Mimic;

import gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.OsirisMimicPanel;
import gtc.DSL.DAF.CORBAServices;
import gtc.DSL.DAF.NotAvailable;
import gtc.DSL.Debug.Output;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public class OsirisMimicScreen extends JFrame 
{
	  public OsirisMimicScreen()
	  {
	    super("Osiris Mimic");
	    
	    this.setLayout(new BorderLayout());
	    OsirisMimicPanel pointingPane = new OsirisMimicPanel();
	    this.add(pointingPane, BorderLayout.CENTER);
	    
	    GraphicsConfiguration  gc = this.getGraphicsConfiguration();
	    Dimension dim = new Dimension(1920, 1160);
	    
	    this.setSize(dim);    
	    this.setResizable(false);
	    this.setUndecorated(true);        
	    this.setVisible(true);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    
	    
	    GraphicsDevice dev = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();     
	    if (dev.isFullScreenSupported())
	    {
	      Output.debug("Full Screen Mode supported");
	      dev.setFullScreenWindow(this);
	    }
	  }
	  
	  /** Overridden so we can show the  shutdown dialog */
	  protected void processWindowEvent(WindowEvent e)
	  {
	    if (e.getID() == WindowEvent.WINDOW_CLOSING)
	    {      
	      String msg = "Are you sure you want to close the OE Screen?";
	      Object[] options = {"Yes",  "Cancel"};
	      int returnValue = JOptionPane.showOptionDialog(this, 
	                                                  msg, 
	                                                 "Osiris Mimic Screen Shutdown",
	                                                  JOptionPane.YES_NO_OPTION,
	                                                  JOptionPane.QUESTION_MESSAGE,
	                                                  null,     //don't use a custom Icon
	                                                  options,  //the titles of buttons
	                                                  options[0]);
	      
	      if (returnValue == JOptionPane.OK_OPTION)
	        super.processWindowEvent(e);
	    }
	    else 
	    {
	      super.processWindowEvent(e);
	    }   
	  }
	  
	  
	  public static void main(String[] args)
	  {
	    for (int i = 0; i < args.length; i++)
	    {
	      if      (args[i].compareTo("-verbose"    )==0) Output.enableVerbose();
	      else if (args[i].compareTo("-debug"      )==0) Output.enableDebug();
	    }
	    String traces = System.getenv("GCS_VERBOSE");
	    Output.setMode(traces);

	    String ns = "corbaname::" + System.getenv("NS_HOST")  + ":" + System.getenv("NS_PORT");
	    String ir = "users/" + System.getProperty("user.name") +  "/InterfaceRepository";      

	    try{
	      CORBAServices.init(ns, ir);
	    }catch(NotAvailable ex){
	      Output.error("Cannot initialiazed CORBA Services. Make sure environment" +
	                   " variables 'NS_HOST' and 'NS_PORT' are set.", ex);
	      System.exit(0);
	    }    
	    
	    SwingUtilities.invokeLater(new Runnable() {
	      public void run() {
	         new OsirisMimicScreen();
	      }
	    });
	  }
	  

	}
