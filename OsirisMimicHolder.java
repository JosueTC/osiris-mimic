package gtc.AL.Inspector.Panels.OSIRIS.Mimic;

import gtc.AL.Inspector.Panels.Common.ComponentFoldersHolder;

public class OsirisMimicHolder extends ComponentFoldersHolder
{
  public OsirisMimicHolder(String component)
  {      
    super(component);
    

    OsirisMimicFolder pane = new OsirisMimicFolder();
    pane.setName("OsirisMimic");
    this.panels_.add(pane);
  }
}
