package gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels;

import gtc.AL.Inspector.Panels.OSIRIS.Mimic.ComponentsOsirisMimicId;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.GUIConstants;
import gtc.DSL.DAF.Services.DataConsumer;
import gtc.DSL.DAF.Services.DataViewer;
import gtc.DSL.GUI_Kit.GUIBasics.Capsules.LabelCapsule;
import gtc.DSL.Types.IntPair;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JPanel;

import DGT.DoubleArray;

/**
 * Represent the Status of the CCD. The CCD can be Idle, TranferingCharge, Readingout or exposing.
 * 
 * @author mhuertas
 *
 */
public class CCDWindowsPanel extends JPanel implements Runnable {

	private DataConsumer currentWindow1Position_XConsumer_;
	private DataConsumer currentWindow1Position_YConsumer_;				
	private DataConsumer currentWindowSize_XConsumer_;				
	private DataConsumer currentWindowSize_YConsumer_;
	
	private LabelCapsule windowTitleLabel;
	
	private LabelCapsule windowPosLabel;
	private LabelCapsule windowSizeLabel;

	private LabelCapsule windowPosXLabel;
	private LabelCapsule windowPosYLabel;

	private LabelCapsule windowSizeXLabel;
	private LabelCapsule windowSizeYLabel;
	
	
	private int currentWindowPosition1X_;
	private int currentWindowPosition1Y_;
	private int currentWindowSizeX_;
	private int currentWindowSizeY_;

	private GridBagLayout gb;

	private enum MONITORSCALAR {currentWindow1Position_X,
					currentWindow1Position_Y,
					currentWindowSize_X,
					currentWindowSize_Y};

	public CCDWindowsPanel() {
		this.currentWindow1Position_XConsumer_=new DataConsumer(ComponentsOsirisMimicId.DATAACQUISITIONCOMPONENT,"currentWindowPosition1X");
		this.currentWindow1Position_XConsumer_.setViewer(new MonitorsScalarConsumer(MONITORSCALAR.currentWindow1Position_X));

		this.currentWindow1Position_YConsumer_=new DataConsumer(ComponentsOsirisMimicId.DATAACQUISITIONCOMPONENT,"currentWindowPosition1Y");
		this.currentWindow1Position_YConsumer_.setViewer(new MonitorsScalarConsumer(MONITORSCALAR.currentWindow1Position_Y));

		this.currentWindowSize_XConsumer_=new DataConsumer(ComponentsOsirisMimicId.DATAACQUISITIONCOMPONENT,"currentWindowSizeX");
		this.currentWindowSize_XConsumer_.setViewer(new MonitorsScalarConsumer(MONITORSCALAR.currentWindowSize_X));

		this.currentWindowSize_YConsumer_=new DataConsumer(ComponentsOsirisMimicId.DATAACQUISITIONCOMPONENT,"currentWindowSizeY");
		this.currentWindowSize_YConsumer_.setViewer(new MonitorsScalarConsumer(MONITORSCALAR.currentWindowSize_Y));

		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		gb = new GridBagLayout();
		setLayout(gb);
		
		windowTitleLabel = new LabelCapsule("Window");
		windowTitleLabel.setFont(GUIConstants.FONT_);
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth=3;
		add(windowTitleLabel,c);


		windowPosLabel = new LabelCapsule("Position");
		windowPosLabel.setFont(GUIConstants.FONT_);
		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth=1;
		
		add(windowPosLabel,c);

		windowPosXLabel = new LabelCapsule("");
		windowPosXLabel.setFont(GUIConstants.FONT_);
		
		c.gridx = 1;
		c.gridy = 1;
		add(windowPosXLabel,c);

		windowPosYLabel = new LabelCapsule("");
		windowPosYLabel.setFont(GUIConstants.FONT_);
		c.gridx = 2;
		c.gridy = 1;
		add(windowPosYLabel,c);

		windowSizeLabel = new LabelCapsule("Size");
		windowSizeLabel.setFont(GUIConstants.FONT_);
		c.gridx = 0;
		c.gridy = 2;
		add(windowSizeLabel,c);

		windowSizeXLabel = new LabelCapsule("");
		windowSizeXLabel.setFont(GUIConstants.FONT_);
		c.gridx = 1;
		c.gridy = 2;
		add(windowSizeXLabel,c);

		windowSizeYLabel = new LabelCapsule("");
		windowSizeYLabel.setFont(GUIConstants.FONT_);
		c.gridx = 2;
		c.gridy = 2;
		add(windowSizeYLabel,c);
		
		Thread t = new Thread(this);
		t.start();

	}

	class MonitorsScalarConsumer implements DataViewer
	{
		private MONITORSCALAR monitor_;
		
		public MonitorsScalarConsumer(MONITORSCALAR monitor)
		{	
			this.monitor_=monitor;
		}

		@Override
		public void receiveData(double sample, long timeStamp) {
			switch (monitor_)
			{

				case currentWindow1Position_X:
						if(Math.abs(currentWindowPosition1X_-(int)sample)<1e-6) {
							windowPosXLabel.setLabel(Integer.toString(currentWindowPosition1X_));
						}
    					currentWindowPosition1X_=(int)(sample);
						
					break;
				case currentWindow1Position_Y:
						if(Math.abs(currentWindowPosition1Y_-(int)sample)<1e-6) {
							windowPosYLabel.setLabel(Integer.toString(currentWindowPosition1Y_));
						}
						currentWindowPosition1Y_=(int)(sample);
					break;
				case currentWindowSize_X:
						if(Math.abs(currentWindowSizeX_-(int)sample)<1e-6) {
							windowSizeXLabel.setLabel(Integer.toString(currentWindowSizeX_));
						}
						currentWindowSizeX_=(int)(sample);
					break;
				case currentWindowSize_Y:
						if(Math.abs(currentWindowSizeY_-(int)sample)<1e-6) {
							windowSizeYLabel.setLabel(Integer.toString(currentWindowSizeY_));
						}
						currentWindowSizeY_=(int)(sample);
					break;
			}
		}
		
		@Override
		public void receiveData(DoubleArray sample, long timeStamp) {
		}
		@Override
		public void receiveData(DoubleArray sample,long index,IntPair pair) {
		}	

		@Override
		public void propertyChange(int id, Object oldValue, Object newValue) {
		}
	}
	
	@Override
	public void run() {
	   while(true) {
		   try {
			   Thread.sleep(5000);
		   } catch(Exception e) {};
		   
		   if (currentWindowPosition1X_==0 &&  
	           currentWindowPosition1Y_==0 && 
	           currentWindowSizeX_==2098 && 
	           currentWindowSizeY_==4102) {
			   if (isVisible())
				   this.setVisible(false);
		   } else {
			   if (!isVisible())
				   this.setVisible(true);
		   }
	   }
	}
}
