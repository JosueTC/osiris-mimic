package gtc.AL.Inspector.Panels.OSIRIS.Mimic.CheckConfiguration;

import gtc.AL.Inspector.Panels.OSIRIS.Mimic.ComponentsOsirisMimicId;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.ComponentsIDs;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.OsirisMimicPanel;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.StateComponent;
import gtc.DSL.DAF.Services.DataConsumer;
import gtc.DSL.DAF.Services.DataViewer;
import gtc.DSL.DAF.Services.EnumConsumer;
import gtc.DSL.DAF.Services.EnumViewer;
import gtc.DSL.Types.IntPair;

import java.util.Map;
import java.util.Vector;

import DATAACQUISITIONCOMPONENT.GAIN;
import DATAACQUISITIONCOMPONENT.ReadoutSpeed;
import DGT.DoubleArray;

/**
 * It detects if the current configuration is forgiven, marking the elements
 * in conflict. 
 * 
 * @author mhuertas
 *
 */
public class FiltersConflict {

		
	private enum MONITORBOOLEAN {
		dasSpeed,
		dasGain,
		isSDSUReconfigureOn 
	};

	
	private enum MONITORSCALAR {
		grismPosition,
		filter1Position,
		filter2Position,
		filter3Position
	};

	private Map<String,StateComponent> components_;
	
	private ReadoutSpeed readoutSpeed_;
	private GAIN gain_;
	private boolean isSDSUReconfigureOn_;

	private EnumConsumer dasSpeedConsumer_;
	private EnumConsumer dasGainConsumer_;
	private EnumConsumer isSDSUConfiguredConsumer_;
	
	private DataConsumer grismPositionConsumer_;
	private int grismPosition_;

	private DataConsumer filter1PositionConsumer_;
	private DataConsumer filter2PositionConsumer_;
	private DataConsumer filter3PositionConsumer_;

	private int filter1Position_;
	private int filter2Position_;
	private int filter3Position_;

	private Object[] filter1ElementTable;
	private Object[] filter2ElementTable;
	private Object[] filter3ElementTable;
	private Object[] grismElementTable;

	private OsirisMimicPanel OsirisMimicPanel;

	private String[] booleanLabels = {"True","False"};

	public FiltersConflict(Map<String,StateComponent> components_,
							Object[] filter1ElementTable,
							Object[] filter2ElementTable,
							Object[] filter3ElementTable,
							Object[] grismElementTable,
							OsirisMimicPanel OsirisMimicPanel)
	{
		this.components_=components_;
		this.OsirisMimicPanel=OsirisMimicPanel;
		this.filter1ElementTable=filter1ElementTable;
		this.filter2ElementTable=filter2ElementTable;
		this.filter3ElementTable=filter3ElementTable;
		this.grismElementTable=grismElementTable;

		createMonitors();
	}
	
	private void createMonitors()
	{
		dasSpeedConsumer_ = new EnumConsumer(ComponentsOsirisMimicId.CAMERA,"currentReadoutSpeed");
		dasGainConsumer_ = new EnumConsumer(ComponentsOsirisMimicId.CAMERA,"currentGAIN");
		isSDSUConfiguredConsumer_ = new EnumConsumer(ComponentsOsirisMimicId.CAMERA,"isSDSUReconfigureOn");

		dasSpeedConsumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.dasSpeed));
		dasGainConsumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.dasGain));
		isSDSUConfiguredConsumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.isSDSUReconfigureOn));
		
		filter1PositionConsumer_= new DataConsumer(ComponentsOsirisMimicId.WHEELCOMPONENT_1,"currentPosition");
		filter2PositionConsumer_= new DataConsumer(ComponentsOsirisMimicId.WHEELCOMPONENT_2,"currentPosition");
		filter3PositionConsumer_= new DataConsumer(ComponentsOsirisMimicId.WHEELCOMPONENT_3,"currentPosition");
		grismPositionConsumer_ = new DataConsumer(ComponentsOsirisMimicId.WHEELCOMPONENT_4,"currentPosition");
		
		filter1PositionConsumer_.setViewer(new MonitorsScalarConsumer(MONITORSCALAR.filter1Position));
		filter2PositionConsumer_.setViewer(new MonitorsScalarConsumer(MONITORSCALAR.filter2Position));
		filter3PositionConsumer_.setViewer(new MonitorsScalarConsumer(MONITORSCALAR.filter3Position));
		grismPositionConsumer_.setViewer(new MonitorsScalarConsumer(MONITORSCALAR.grismPosition));
	}
	
	
	class MonitorsBooleanConsumer implements EnumViewer
	{
		private MONITORBOOLEAN monitor_;
		
		public MonitorsBooleanConsumer(MONITORBOOLEAN monitor)
		{	
			this.monitor_=monitor;
		}

		@Override
		public void receiveMagnitudeChange(String component, String magnitude,int newState) {
		

			try{

				switch(monitor_)
				{
					case dasSpeed:
						readoutSpeed_= DATAACQUISITIONCOMPONENT.ReadoutSpeed.from_int(newState);
						break;
					case dasGain:
						gain_= DATAACQUISITIONCOMPONENT.GAIN.from_int(newState);
						break;
					case isSDSUReconfigureOn:

						isSDSUReconfigureOn_ = new Boolean( booleanLabels[newState] );
						break;
						
				}
			

				check();
			} catch(Exception e)
			{
				e.printStackTrace();
			};
		}

		@Override
		public void propertyChange(int id, java.lang.Object oldValue,
				java.lang.Object newValue) {
		}
	}
	
	class MonitorsScalarConsumer implements DataViewer
	{

		private MONITORSCALAR monitor_;
		
		public MonitorsScalarConsumer(MONITORSCALAR  monitor) {
			monitor_ = monitor;
		}

		@Override
		public void receiveData(double sample, long timeStamp) {
			switch (monitor_)
			{
				case grismPosition:
					grismPosition_ = (int) sample;	
					break;
				case filter1Position:
					filter1Position_ = (int) sample;
					break;
				case filter2Position:
					filter2Position_= (int) sample;
					break;
				case filter3Position:
					filter3Position_= (int) sample;
					break;
			}
			try{
				check();
			} catch(Exception e)
			{
			};
		}

		
		
		@Override
		public void receiveData(DoubleArray sample, long timeStamp) {
		}

		@Override
		public void receiveData(DoubleArray sample,long index,IntPair pair) {
		}	


		@Override
		public void propertyChange(int id, java.lang.Object oldValue,
				java.lang.Object newValue) {
		}

	}

	private void check()
	{
		
		
		
		Vector<StateComponent> inConflict = new Vector<StateComponent>();
		Vector<StateComponent> noConflict = new Vector<StateComponent>();
	
		String grismName = (String)grismElementTable[grismPosition_-1];
		String filter1Name = (String)filter1ElementTable[filter1Position_-1];
		String filter2Name = (String)filter2ElementTable[filter2Position_-1];
		String filter3Name = (String)filter3ElementTable[filter3Position_-1];
		
		
		
		noConflict.add(components_.get(ComponentsIDs.DATAACQUISITIONCOMPONENT));
		noConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT1));
		noConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT2));
		noConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT3));
		noConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT4));
		noConflict.add(components_.get(ComponentsIDs.TUNABLEFILTERSCOMPONENT1));
		noConflict.add(components_.get(ComponentsIDs.TUNABLEFILTERSCOMPONENT2));

		
		if ( !isSDSUReconfigureOn_ ) {
			inConflict.add(components_.get(ComponentsIDs.DATAACQUISITIONCOMPONENT));
		}		 
		 
		
		if (grismName.indexOf("TF_") != -1) {
			//Rueda 4 TF puesto + DAS Speed 100 =>  Rueda 4 (TF) y DAS
			//Rueda 4 TF puesto + DAS Gain 4.75
			if(readoutSpeed_== DATAACQUISITIONCOMPONENT.ReadoutSpeed.SLOW_SPEED3 || 
					gain_ == DATAACQUISITIONCOMPONENT.GAIN.GAIN_x4_75) {
				inConflict.add(components_.get(ComponentsIDs.DATAACQUISITIONCOMPONENT));
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT4));
				inConflict.add(components_.get(ComponentsIDs.TUNABLEFILTERSCOMPONENT1));
				inConflict.add(components_.get(ComponentsIDs.TUNABLEFILTERSCOMPONENT2));
				
				

			}
			//Rueda 4 TF puesto + Ningun filtro
			if (((filter1Name.indexOf("OPEN")!=-1) &&
					(filter2Name.indexOf("OPEN")!=-1) && 
					(filter3Name.indexOf("OPEN")!=-1)) ) {

				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT1));
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT2));
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT3));
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT4));
				inConflict.add(components_.get(ComponentsIDs.TUNABLEFILTERSCOMPONENT1));
				inConflict.add(components_.get(ComponentsIDs.TUNABLEFILTERSCOMPONENT2));
			}
			//Rueda 4 TF puesto + Filtro Sloa
			if( (filter2Name.indexOf("Sloan")!=-1) )
			{
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT2));
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT4));
				inConflict.add(components_.get(ComponentsIDs.TUNABLEFILTERSCOMPONENT1));
				inConflict.add(components_.get(ComponentsIDs.TUNABLEFILTERSCOMPONENT2));
			}
			//Rueda 4 TF + UCM
			if( (filter1Name.charAt(0) =='U') )
			{
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT1));
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT4));
				inConflict.add(components_.get(ComponentsIDs.TUNABLEFILTERSCOMPONENT1));
				inConflict.add(components_.get(ComponentsIDs.TUNABLEFILTERSCOMPONENT2));
			}
			if( (filter2Name.charAt(0) =='U') )
			{
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT2));
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT4));
				inConflict.add(components_.get(ComponentsIDs.TUNABLEFILTERSCOMPONENT1));
				inConflict.add(components_.get(ComponentsIDs.TUNABLEFILTERSCOMPONENT2));
			}
			if( (filter3Name.charAt(0) =='U') )
			{
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT3));
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT4));
				inConflict.add(components_.get(ComponentsIDs.TUNABLEFILTERSCOMPONENT1));
				inConflict.add(components_.get(ComponentsIDs.TUNABLEFILTERSCOMPONENT2));
			}

			
			//Rueda 4 TF puesto + GR
			if( (filter2Name.indexOf("GR")!=-1) )
			{
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT2));
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT4));
				inConflict.add(components_.get(ComponentsIDs.TUNABLEFILTERSCOMPONENT1));
				inConflict.add(components_.get(ComponentsIDs.TUNABLEFILTERSCOMPONENT2));

			}

		} else if (grismName.charAt(0) == 'R') {
			//Rueda 4 Grisma puesto + DAS Speed 100
			//Rueda 4 Grisma puesto + DAS Gain 4.75
			if(readoutSpeed_== DATAACQUISITIONCOMPONENT.ReadoutSpeed.SLOW_SPEED3 || 
					gain_ == DATAACQUISITIONCOMPONENT.GAIN.GAIN_x4_75) {
				
				inConflict.add(components_.get(ComponentsIDs.DATAACQUISITIONCOMPONENT));
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT4));

			}
		} else {
			//Rueda 4 No Grisma ni TF (rueda Grisma OPEN) + DAS Speed 100
			//Rueda 4 No Grisma ni TF (rueda Grisma OPEN) + DAS Gain 4.75
			if(readoutSpeed_== DATAACQUISITIONCOMPONENT.ReadoutSpeed.SLOW_SPEED3 || 
					gain_ == DATAACQUISITIONCOMPONENT.GAIN.GAIN_x4_75) {
				
								
				inConflict.add(components_.get(ComponentsIDs.DATAACQUISITIONCOMPONENT));
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT4));
			}
		}
		
		//2 filtros a la vez en la ruedas de filtros 
		if (!(filter1Name.indexOf("OPEN")!=-1) && !(filter2Name.indexOf("OPEN")!=-1))
		{
								
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT1));
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT2));

		}
		//2 filtros a la vez en la ruedas de filtros 	
		if (!(filter1Name.indexOf("OPEN")!=-1) && !(filter3Name.indexOf("OPEN")!=-1))
		{
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT1));
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT3));
		}
		//2 filtros a la vez en la ruedas de filtros 
		if (!(filter2Name.indexOf("OPEN")!=-1) && !(filter3Name.indexOf("OPEN")!=-1))
		{
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT2));
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT3));
		}
		//3 filtros a la vez en la rueda de filtros
		if (!(filter1Name.indexOf("OPEN")!=-1) && 
				!(filter2Name.indexOf("OPEN")!=-1) &&
				!(filter3Name.indexOf("OPEN")!=-1))
		{
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT1));
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT2));
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT3));
		}

		//Rueda 4 Grismas R300R, R500R o R1000R o R2500R o R2500I + Ningun filtro => Rueda 4 (Grismas) + ruedas de filtros
		//Rueda 4 Grismas R300R, R500R o R1000R o R2500R o R2500I + Filtro Sloan => Rueda 4 (Grismas) + ruedas de filtros
		//Rueda 4 Grismas R300R, R500R o R1000R o R2500R o R2500I + Filtro OS => Rueda 4 (Grismas) + ruedas de filtros
		//Un filtro UCM + Grisma Rojos
		if ( (grismName.indexOf("R300R") != -1) || 
			 (grismName.indexOf("R500R") != -1) || 
			 (grismName.indexOf("R1000R") != -1) || 
			 (grismName.indexOf("R2500R") != -1 ) || 
			 (grismName.indexOf("R2500I") != -1 ) )
		{
			if ((filter1Name.indexOf("OPEN")!=-1) && 
					(filter2Name.indexOf("OPEN")!=-1) &&
					(filter3Name.indexOf("OPEN")!=-1) )
			{
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT1));
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT2));
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT3));
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT4));
			}

			if ((filter2Name.indexOf("Sloan")!=-1) || 
					( filter1Name.charAt(0) == 'f') ||
					( filter1Name.charAt(0) == 'U') )
			{
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT1));
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT4));
			}
			
			if ((filter2Name.indexOf("Sloan")!=-1) || ( 
					filter2Name.charAt(0) == 'f') ||
					( filter2Name.charAt(0) == 'U') )
			{
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT2));
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT4));
			}

			if ((filter2Name.indexOf("Sloan")!=-1) || ( 
					filter3Name.charAt(0) == 'f') ||
					( filter3Name.charAt(0) == 'U') )
			{
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT3));
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT4));
			}
		}	
		
		//Rueda 4 Grismas R300B, R500B, R1000B, R2000B, R2500U, R2500V + un filtro (el que sea) => Rueda 4 (Grismas) + ruedas de filtros 
		if ( (grismName.indexOf("R300B") != -1) || 
					 (grismName.indexOf("R500B") != -1) || 
					 (grismName.indexOf("R1000B") != -1) || 
					 (grismName.indexOf("R2000B") != -1) ||
					 (grismName.indexOf("R2500U") != -1) ||
					 (grismName.indexOf("R2500V") != -1) )
		{

			if ((filter1Name.indexOf("OPEN")==-1) )
			{
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT1));
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT4));
			}

			if ((filter2Name.indexOf("OPEN")==-1) )
			{
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT2));
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT4));
			}

			if ((filter3Name.indexOf("OPEN")==-1) )
			{
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT3));
				inConflict.add(components_.get(ComponentsIDs.WHEELCOMPONENT4));
			}
		}
		
		for (StateComponent component : noConflict )
			component.setBadConfiguration(false);
		
		for (StateComponent component : inConflict)
			component.setBadConfiguration(true);
		
		/*if (inConflict.size() != 0 )
			OsirisMimicPanel.setBadConfiguration(true);
		else
			OsirisMimicPanel.setBadConfiguration(false);*/
	}
}

