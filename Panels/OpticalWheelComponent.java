package gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels;

import gtc.AL.Inspector.Panels.OSIRIS.Mimic.GUIConstants;
import gtc.DSL.DAF.Services.DataConsumer;
import gtc.DSL.DAF.Services.DataViewer;
import gtc.DSL.DAF.Services.EnumConsumer;
import gtc.DSL.DAF.Services.EnumViewer;
import gtc.DSL.GUI_Kit.GUIBasics.BasicsGUIDefaults;
import gtc.DSL.GUI_Kit.GUIBasics.Capsules.LabelCapsule;
import gtc.DSL.Types.IntPair;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.geom.Point2D;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

import DGT.DoubleArray;
import WHEELCOMPONENT.wheelStat;


/**
 * Represents the Optical Wheel. 
 * 
 * An optical wheel contains a named positions and a kind of sketch for 
 * the optical components inside of it.
 * 
 * @author mhuertas
 *
 */
public class OpticalWheelComponent extends StateComponent implements EnumViewer, DataViewer {
	private static final long serialVersionUID = 1L;
	private wheelStat stateWheel_;
	private EnumConsumer stateWheelConsumer_;
	private DataConsumer currentPositionConsumer_;
	private int currentPosition_=-1;
	private LabelCapsule positionName;
	private Object[] opticalElementTable;
	private Point2D connectionPoint;
	private JLabel opticalSketch;
	private static final String OPEN = "OPEN";


	public OpticalWheelComponent(String component, String horientation,JLabel opticalSketch,Object[] opticalElementTable) {
		super(component);
		
		this.setOpaque(false);

		this.opticalSketch=opticalSketch;
		this.stateWheelConsumer_= new EnumConsumer(component,"wheelStatus");
		this.stateWheelConsumer_.setViewer(this);
		this.currentPositionConsumer_= new DataConsumer(component,"currentPosition");
		this.currentPositionConsumer_.setViewer(this);
		this.opticalElementTable=opticalElementTable;

		
		this.setLayout(new BorderLayout(1,1));
		this.setBackground(BasicsGUIDefaults.DARKER_GRAY);
		this.positionName = new LabelCapsule("N/A");
		this.positionName.setFont(GUIConstants.FONT_);

		positionName.setBorder(BorderFactory.createEmptyBorder(2,2, 2, 2));
		add(positionName,horientation);
		add(opticalSketch,BorderLayout.EAST);
		
		this.addComponentListener(this);
		
	}

	public Point2D getConnectionPoint() {
		return connectionPoint;
	}
	
	public String getPositionName()
	{
		try
		{
			return  (String)opticalElementTable[currentPosition_];
		} catch(ArrayIndexOutOfBoundsException e)
		{
			return "N/A";
		}
	}

	@Override
	public Status getStatus() {
		if (stateWheel_ == wheelStat.UNKNOWN )
			return Status.ERROR;
		if ((stateWheel_ == wheelStat.HOMING || 
				stateWheel_ == wheelStat.INITIALIZING || 
				stateWheel_ == wheelStat.MOVING || 
				stateWheel_ == wheelStat.STOPING))
			return Status.MOVING;
		
		return Status.READY;
	}
	

	@Override
	public void receiveMagnitudeChange(String component, String magnitude,
			int newState) {
		try 
		{

			this.stateWheel_= WHEELCOMPONENT.wheelStat.from_int(newState);
			selectBorder();
		} 
		catch(Exception e)
		{
			e.printStackTrace();
		}


	}
	@Override
	public void receiveData(double sample, long timeStamp) {
		currentPosition_=(int)sample-1;
		String position= getPositionName();
				
		positionName.setLabel(position);
		if ( position.equals(OPEN) )  
			this.opticalSketch.setVisible(false);
		else 
			this.opticalSketch.setVisible(true);
	}
	@Override
	public void receiveData(DoubleArray sample, long timeStamp) {
	}
	
	@Override
	public void receiveData(DoubleArray sample,long index,IntPair pair) {
	}	


	
	public void updatePosition(double x, double y, double width,double height)
	{
		connectionPoint= new Point2D.Double(x+width*0.8,
												y+height/2.0);
		
		this.opticalSketch.setPreferredSize(new Dimension((int)(width*0.3),opticalSketch.getHeight()));
		
		//this.positionName.setMaximumSize(new Dimension((int)(width*0.7),(int)(height*0.5)));
	    //this.positionName.setPreferredSize(new Dimension((int)(width*0.7),(int)(height*0.5)));

	}

}

