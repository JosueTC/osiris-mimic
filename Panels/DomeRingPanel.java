package gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels;

import gtc.AL.Inspector.Panels.OSIRIS.Mimic.ComponentsOsirisMimicId;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.Sketch.ICMDomeLampGroupSketch;
import gtc.DSL.DAF.Services.DataConsumer;
import gtc.DSL.DAF.Services.DataViewer;
import gtc.DSL.DAF.Services.EnumConsumer;
import gtc.DSL.DAF.Services.EnumViewer;
import gtc.DSL.Types.IntPair;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.Vector;

import javax.swing.JPanel;

import DGT.DoubleArray;

public class DomeRingPanel extends JPanel  {
	private static final long serialVersionUID = 1L;

	private enum MONITORBOOLEAN {isOnGroup1,
									isOnGroup2,
									isOnGroup3};
									
	private enum MONITORSCALAR {intensityLightGroup1,
									intensityLightGroup2,
									intensityLightGroup3};

	
	private Vector<ICMDomeLampGroupSketch> iCMLampGroups;
		
	private EnumConsumer iCMLampisOnGroup1Consumer_;
	private EnumConsumer iCMLampisOnGroup2Consumer_;
	private EnumConsumer iCMLampisOnGroup3Consumer_;
	
	private DataConsumer iCMLampIntensityLightGroup1_;
	private DataConsumer iCMLampIntensityLightGroup2_;
	private DataConsumer iCMLampIntensityLightGroup3_;
	private RenderingHints renderingHints;

	private ICMDomeLampGroupSketch iCMLampGroup1;
	private ICMDomeLampGroupSketch iCMLampGroup2;
	private ICMDomeLampGroupSketch iCMLampGroup3;
		
	public DomeRingPanel() {
		this.setLayout(null);
		
		this.iCMLampGroup1 = new ICMDomeLampGroupSketch(0);
	    this.iCMLampisOnGroup1Consumer_ = new EnumConsumer(ComponentsOsirisMimicId.ICMDOME, "isOn_Group1");
	    this.iCMLampisOnGroup1Consumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.isOnGroup1));
	    this.iCMLampIntensityLightGroup1_ = new DataConsumer(ComponentsOsirisMimicId.ICMDOME,"intensityLight_Group1");
	    this.iCMLampIntensityLightGroup1_.setViewer(new MonitorsScalarConsumer(MONITORSCALAR.intensityLightGroup1));
		
		this.iCMLampGroup2 = new ICMDomeLampGroupSketch(-Math.PI/2.0);
	    this.iCMLampisOnGroup2Consumer_ = new EnumConsumer(ComponentsOsirisMimicId.ICMDOME, "isOn_Group2");
	    this.iCMLampisOnGroup2Consumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.isOnGroup2));
	    this.iCMLampIntensityLightGroup2_ = new DataConsumer(ComponentsOsirisMimicId.ICMDOME,"intensityLight_Group2");
	    this.iCMLampIntensityLightGroup2_.setViewer(new MonitorsScalarConsumer(MONITORSCALAR.intensityLightGroup2));
		
		this.iCMLampGroup3 = new ICMDomeLampGroupSketch(Math.PI/2.0);
	    this.iCMLampisOnGroup3Consumer_ = new EnumConsumer(ComponentsOsirisMimicId.ICMDOME, "isOn_Group3");
	    this.iCMLampisOnGroup3Consumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.isOnGroup3));
	    this.iCMLampIntensityLightGroup3_ = new DataConsumer(ComponentsOsirisMimicId.ICMDOME,"intensityLight_Group3");
	    this.iCMLampIntensityLightGroup3_.setViewer(new MonitorsScalarConsumer(MONITORSCALAR.intensityLightGroup3));

		
		iCMLampGroups = new Vector<ICMDomeLampGroupSketch>();
		iCMLampGroups.add(iCMLampGroup1);
		iCMLampGroups.add(iCMLampGroup2);
		iCMLampGroups.add(iCMLampGroup3);

		renderingHints = new RenderingHints(
				RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
	}
	
	private void setupGUI(int x, int y, int w, int h)  {
		int sizeX=400;
		int sizeY=50;
		iCMLampGroup1.setLocation((int)((w-sizeX)*0.5),20);
		iCMLampGroup1.setSize(sizeX,sizeY);
		
		iCMLampGroup2.setLocation(10,(int)((h-sizeX)*0.5)+20);
		iCMLampGroup2.setSize(sizeY,sizeX);

		iCMLampGroup3.setLocation(w-sizeY,(int)((h-sizeX)*0.5)+20);
		iCMLampGroup3.setSize(sizeY,sizeX);
	}
	 
	@Override
	public void paintComponent(Graphics g)  {
		Graphics2D g2d = (Graphics2D)g;
		g2d.setRenderingHints(renderingHints);
		
		double x = this.getLocation().getX();
		double y = this.getLocation().getY();
	    	double width = this.getSize().getWidth();
	    	double height = this.getSize().getHeight();
	    
	   	 setupGUI((int)x,(int)y,(int)width,(int)height);
		
		for (ICMDomeLampGroupSketch iCMLampGroup : iCMLampGroups) {
			iCMLampGroup.paint(g2d);
		}
	}
	
	class MonitorsBooleanConsumer implements EnumViewer {
		private MONITORBOOLEAN monitor_;
		private String [] isOnGroup1Labels;
		private String [] isOnGroup2Labels;
		private String [] isOnGroup3Labels;
		
		public MonitorsBooleanConsumer(MONITORBOOLEAN monitor) {	
			this.monitor_=monitor;
		}

		@Override
		public void receiveMagnitudeChange(String component, String magnitude,int newState) {
			try  {
				switch (monitor_)
				{
					
					case isOnGroup1:
						Boolean isOnGroup1 = new Boolean( isOnGroup1Labels[newState] );
						ICMDomeLampGroupSketch iCMDomeLampGroup1 = iCMLampGroups.get(0);
						if (iCMDomeLampGroup1!=null) {
							if (iCMDomeLampGroup1.getStatus() != isOnGroup1 ) {
								iCMDomeLampGroup1.switchOnOff(isOnGroup1);
								repaint();
							}
						}
							
						
						break;
					case isOnGroup2:
						Boolean isOnGroup2 = new Boolean( isOnGroup2Labels[newState] );
						ICMDomeLampGroupSketch iCMDomeLampGroup2 = iCMLampGroups.get(1);
						if (iCMDomeLampGroup2!=null)  {
							if (iCMDomeLampGroup2.getStatus() != isOnGroup2 ) {
								iCMDomeLampGroup2.switchOnOff(isOnGroup2);
								repaint();
							}
						}
						break;
					case isOnGroup3:
						Boolean isOnGroup3 = new Boolean( isOnGroup3Labels[newState] );
						ICMDomeLampGroupSketch iCMDomeLampGroup3 = iCMLampGroups.get(2);
						if (iCMDomeLampGroup3!=null) {
							if (iCMDomeLampGroup3.getStatus() != isOnGroup3 ) {
								iCMDomeLampGroup3.switchOnOff(isOnGroup3);
								repaint();
							}
						}
						break;
				}
				
			} 
			catch(Exception e)
			{
				e.printStackTrace();
			}

		}

		@Override
		public void propertyChange(int id, Object oldValue, Object newValue) {
			switch (monitor_)
			{
				case isOnGroup1:
					isOnGroup1Labels=(String[])newValue;
					break;
				case isOnGroup2:
					isOnGroup2Labels=(String[])newValue;
					break;
				case isOnGroup3:
					isOnGroup3Labels=(String[])newValue;
					break;
			}
			
		}
		
	}
	
	class MonitorsScalarConsumer implements DataViewer {
		private MONITORSCALAR monitor_;
		
		public MonitorsScalarConsumer(MONITORSCALAR monitor) {
			this.monitor_=monitor;
		}
		
		@Override
		public void receiveData(double sample, long timeStamp) {
			try {
				switch (monitor_)
				{
					case intensityLightGroup1:
						ICMDomeLampGroupSketch iCMDomeLampGroup1 = iCMLampGroups.get(0);
						if (iCMDomeLampGroup1!=null)
							if (Math.abs(iCMDomeLampGroup1.getIntensityLight()-sample) > 1e-3 ) {
								iCMDomeLampGroup1.setIntensityLight(sample);
								repaint();
							}
						
						break;
					case intensityLightGroup2:
						ICMDomeLampGroupSketch iCMDomeLampGroup2 = iCMLampGroups.get(1);
						if (iCMDomeLampGroup2!=null)
							if (Math.abs(iCMDomeLampGroup2.getIntensityLight()-sample) > 1e-3 ) {
								iCMDomeLampGroup2.setIntensityLight(sample);
								repaint();
							}

						
						break;
					case intensityLightGroup3:
						ICMDomeLampGroupSketch iCMDomeLampGroup3 = iCMLampGroups.get(2);
						if (iCMDomeLampGroup3!=null)
							if (Math.abs(iCMDomeLampGroup3.getIntensityLight()-sample) > 1e-3 ) {
								iCMDomeLampGroup3.setIntensityLight(sample);
								repaint();
							}

						
						break;
				}

			} 
			catch(Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		public void receiveData(DoubleArray sample, long timeStamp) {
		}

		@Override
		public void receiveData(DoubleArray sample,long index,IntPair pair) {
		}	

		@Override
		public void propertyChange(int id, Object oldValue, Object newValue) {
		}
	}
}
