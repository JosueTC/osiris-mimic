package gtc.AL.Inspector.Panels.OSIRIS.Mimic.Sketch;


import gtc.AL.Inspector.Panels.OSIRIS.Mimic.GUIConstants;
import gtc.DSL.GUI_Kit.GUIBasics.BasicsGUIDefaults;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.awt.geom.RoundRectangle2D;
import java.util.Vector;

import javax.swing.JLabel;

public class ICMDomeLampGroupSketch extends JLabel   
{
	private boolean status=false;
	private double intensityLight=100.0;
	private double rotate=100.0;
	
	
	public ICMDomeLampGroupSketch(double rotate) {
		this.rotate=rotate;
	}

	
	public boolean getStatus() {
		return status;
	}


	
	public void switchOnOff(boolean status) {
		this.status=status;
	}

	public void setIntensityLight(double intensityLight) {
		this.intensityLight=intensityLight;
	}
	
	public double getIntensityLight() {
		return intensityLight;
	}

	public Vector<ICMLampIntensitySketch> getLamps() {
		return null;
	}

	@Override
	public void paintComponent(Graphics g)  {
		Graphics2D g2d =(Graphics2D)g;
		
		
		int X = (int)this.getX();
		int Y = (int)this.getY();
		int W = (int)(this.getWidth());
		int H = (int)(this.getHeight());
		
		
		g2d.setColor(BasicsGUIDefaults.BACKGROUND);
		g2d.setStroke(new BasicStroke(10.0f));
		
		RoundRectangle2D.Double arc = new RoundRectangle2D.Double(X,Y,W,H,8,8);
		g2d.fill(arc);
		
		double factor = Math.min(W,H)*0.15;
		
		RoundRectangle2D.Double intestity = null;
		if (W>H) {
			intestity = new RoundRectangle2D.Double(X+factor,Y+factor,(W-2*factor)*(intensityLight/100.0),H-2*factor,8,8);
		} else {
			intestity = new RoundRectangle2D.Double(X+factor,Y+factor,(W-2*factor),(H-2*factor)*(intensityLight/100.0),8,8);
		}
		g2d.setColor( BasicsGUIDefaults.ORANGE);
		g2d.fill(intestity);
		
		Font font = new Font("Courier", Font.PLAIN, 45);
		String intesityLightLegent = Integer.toString((int)intensityLight)+"%";
		Dimension textSize = getFotSize(intesityLightLegent,font); 
		
		g2d.setColor(Color.WHITE);
		g2d.setFont(font);
		
		label_line(g2d,
				X+(W-textSize.getWidth())*0.5,
				Y+(H+textSize.getHeight()-15)*0.5,
				rotate,
				(int)(X+(W)*0.5),
				(int)(Y+(H)*0.5),
				intesityLightLegent);
		
	}

	private void label_line(Graphics2D g2D, double x, double y, double theta,double px, double py, String label) {

	    AffineTransform fontAT = new AffineTransform();
	    fontAT.setToRotation(theta,px,py);
	    g2D.setTransform(fontAT);
	    g2D.drawString(label, (int)x, (int)y);
	    g2D.setTransform(new AffineTransform());
	}
	
	private Dimension getFotSize(String label, Font font) {
		AffineTransform affinetransform = new AffineTransform();     
		FontRenderContext frc = new FontRenderContext(affinetransform,true,true);    
		int textwidth = (int)(font.getStringBounds(label, frc).getWidth());
		int textheight = (int)(font.getStringBounds(label, frc).getHeight());
		
		return new Dimension(textwidth,textheight);
	}

}
