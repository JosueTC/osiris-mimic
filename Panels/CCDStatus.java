package gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels;

import gtc.AL.Inspector.Panels.OSIRIS.Mimic.ComponentsOsirisMimicId;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.GUIConstants;
import gtc.DSL.DAF.Services.EnumConsumer;
import gtc.DSL.DAF.Services.EnumViewer;
import gtc.DSL.GUI_Kit.GUIBasics.BasicsGUIDefaults;
import gtc.DSL.GUI_Kit.GUIBasics.Capsules.LabelCapsule;
import gtc.DSL.GUI_Kit.ScalarMonitors.ScalarMonitorCapsule;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JPanel;

/**
 * Represent the Status of the CCD. The CCD can be Idle, TranferingCharge, Readingout or exposing.
 * 
 * @author mhuertas
 *
 */

public class CCDStatus extends JPanel {
	private enum MONITORBOOLEAN {
		isExposing,
		isTransferingCharge,
		isReadingout,
		isDetectorControllerIdle
	};
	
	private EnumConsumer isExposingConsumer_;
	private EnumConsumer isTransferingChargeConsumer_;
	private EnumConsumer isReadingoutConsumer_;
	private EnumConsumer isDetectorControllerIdleConsumer_;
	
	
	private LabelCapsule ccdStatus_;
	private ScalarMonitorCapsule ccdElapsedTime_;	
	
	public CCDStatus()
	{
		this.setOpaque(false);
		this.ccdStatus_=new LabelCapsule("EXPOSING");
		this.ccdStatus_.setBackground(BasicsGUIDefaults.BLUE);
		this.ccdStatus_.setFont(GUIConstants.FONT_);
		isExposingConsumer_ = new EnumConsumer(ComponentsOsirisMimicId.DATAACQUISITIONCOMPONENT,"isExposing");
		isTransferingChargeConsumer_ = new EnumConsumer(ComponentsOsirisMimicId.DATAACQUISITIONCOMPONENT,"isTransferingCharge");
		isReadingoutConsumer_ = new EnumConsumer(ComponentsOsirisMimicId.DATAACQUISITIONCOMPONENT,"isReadingout");
		isDetectorControllerIdleConsumer_ = new EnumConsumer(ComponentsOsirisMimicId.DATAACQUISITIONCOMPONENT,"isDetectorControllerIdle");
		
		ccdElapsedTime_ = new ScalarMonitorCapsule("Elapsed",ComponentsOsirisMimicId.DATAACQUISITIONCOMPONENT,"currentElapsedTime");
		ccdElapsedTime_.setFont(GUIConstants.FONT_);
		add(ccdStatus_);
		add(ccdElapsedTime_);		

		isExposingConsumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.isExposing));
		isTransferingChargeConsumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.isTransferingCharge));
		isReadingoutConsumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.isReadingout));
		isDetectorControllerIdleConsumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.isDetectorControllerIdle));
		
		setLayout(new GridLayout(2,1));
		
		ccdStatus_.setVisible(true);
		ccdElapsedTime_.setVisible(true);
	}
	
	class MonitorsBooleanConsumer implements EnumViewer
	{
		private MONITORBOOLEAN monitor_;
		
		private String [] isExposingLabels;
		private String [] isTransferingChargeLabels;
		private String [] isReadingoutLabels;
		private String [] isDetectorControllerIdleLabels;
		
		private boolean  isExposing_ = false;
		private boolean  isTransferingCharge_= false;
		private boolean  isReadingout_= false;
		private boolean  isDetectorControllerIdle_ = false;

		
		public MonitorsBooleanConsumer(MONITORBOOLEAN monitor)
		{	
			this.monitor_=monitor;
		}

		@Override
		public void receiveMagnitudeChange(String component, String magnitude,
				int newState) {

			switch(monitor_)
			{
				case isExposing:
					isExposing_ = new Boolean( isExposingLabels[newState] );
					if ( isExposing_) {
						ccdStatus_.setLabel("EXPOSING");
						ccdStatus_.setBackground(StateComponent.TRANSLUCID_YELLOW);
						ccdStatus_.setForeground(Color.BLACK);
						ccdStatus_.setVisible(true);
						ccdElapsedTime_.setVisible(true);
					}

					break;
				case isTransferingCharge:
					isTransferingCharge_ = new Boolean( isTransferingChargeLabels[newState] );
					if (isTransferingCharge_) {
						ccdStatus_.setLabel("TRANSFERING");
						ccdStatus_.setBackground(StateComponent.TRANSLUCID_YELLOW);						
						ccdStatus_.setForeground(Color.BLACK);
						ccdStatus_.setVisible(true);
						ccdElapsedTime_.setVisible(true);
					}
					
					break;
				case isReadingout:
					isReadingout_ = new Boolean( isReadingoutLabels[newState] );
					if (isReadingout_) {
						ccdStatus_.setLabel("READINGOUT");
						ccdStatus_.setBackground(Color.GREEN);						
						ccdStatus_.setForeground(Color.WHITE);
						ccdStatus_.setVisible(true);
						ccdElapsedTime_.setVisible(true);
					}
					break;
				case isDetectorControllerIdle:
					isDetectorControllerIdle_ = new Boolean( isDetectorControllerIdleLabels[newState] );
					if (isDetectorControllerIdle_) {
						ccdStatus_.setLabel("IDLE");
						ccdStatus_.setVisible(true);
						ccdStatus_.setBackground(BasicsGUIDefaults.BLUE);
						ccdStatus_.setForeground(Color.WHITE);
						ccdElapsedTime_.setVisible(false);
					} else {
						ccdStatus_.setLabel("SETTINGUP");
						ccdStatus_.setBackground(StateComponent.TRANSLUCID_YELLOW);												
						ccdStatus_.setForeground(Color.BLACK);
						ccdElapsedTime_.setVisible(true);
					}
					break;
			}
		}

		@Override
		public void propertyChange(int id, java.lang.Object oldValue,
				java.lang.Object newValue) {
			switch(monitor_)
			{
				case isExposing:
					isExposingLabels = (String[])newValue;
					break;
				case isTransferingCharge:
					isTransferingChargeLabels = (String[])newValue;
					break;
				case isReadingout:
					isReadingoutLabels = (String[])newValue;
					break;
				case isDetectorControllerIdle:
					isDetectorControllerIdleLabels = (String[])newValue;
					break;
					
			}
		}
	}
	

}
