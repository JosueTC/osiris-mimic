package gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels;


import gtc.AL.Inspector.Panels.OSIRIS.Mimic.Sketch.ICMLampIntensitySketch;
import gtc.DSL.GUI_Kit.GUIBasics.BasicsGUIDefaults;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.geom.Arc2D;
import java.util.Vector;

public class ICMDomeLampGroup   
{
	private int X;
	private int Y;
	private int H;
	private int W;
	
	//Vector<ICMLampIntensitySketch> lamps;

	public ICMDomeLampGroup() {
		//lamps= new Vector<ICMLampIntensitySketch>();
	}

	/*public void addLamp(ICMLampIntensitySketch lamp)
	{
		lamps.add(lamp);
	}*/
	
	public void setPosition(int x,int y) {
		this.X = x;
		this.Y = y;
	}
	
	public void setSize(int w,int h) {
		this.W = w;
		this.H = h;
	}

	public void setH(int h) {
		H = h;
	}

	public void setW(int w) {
		W = w;
	}



	public void switchOnOff(boolean status) {
		/*for(ICMLampIntensitySketch lamp:lamps)
		{
			lamp.setOn(status);
		}*/
	}

	public void setIntensityLight(double intensityLight) {
		/*for(ICMLampIntensitySketch lamp:lamps)
		{
			lamp.setIntensityLight(intensityLight);
		}*/
	}
	
	public void paintComponent(Graphics2D g) {
/*		for(ICMLampIntensitySketch lamp:lamps)
		{
			lamp.draw(g);
		}*/
	}

	public Vector<ICMLampIntensitySketch> getLamps() {
		return null;
	}

	public void draw(Graphics2D g2d) {
		g2d.setColor(BasicsGUIDefaults.DARKER_GRAY);
		g2d.setStroke(new BasicStroke(80.0f));
		Arc2D.Double.Double arc = new Arc2D.Double.Double(X,Y,W,H,90,30,0);
		g2d.draw(arc);
	}

}
