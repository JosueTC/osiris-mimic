package gtc.AL.Inspector.Panels.OSIRIS.Mimic.Sketch;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.RoundRectangle2D;

import javax.swing.JLabel;

public class TunnableFilterSketch extends JLabel  {
	private static final long serialVersionUID = 1L;
	private Color color;

	public TunnableFilterSketch(Color color) {
		this.color = color;
		this.setOpaque(false);
	}
	
	public void paintComponent(Graphics2D g) {
	    int width = (int)(this.getVisibleRect().getWidth()*0.75);
	    int height = (int)(this.getVisibleRect().getHeight()*0.5);

		int x = (int)(getVisibleRect().getWidth()/2.0-width/2.0);
		int y = (int)(this.getVisibleRect().getHeight()*0.25);

		g.setColor(color);
		
		RoundRectangle2D.Double shape1 = new RoundRectangle2D.Double(x,y,width,height/4.0,10.5f,10.5f);
		RoundRectangle2D.Double shape2 = new RoundRectangle2D.Double(x,y+3*height/4.0,width,height/4.0,10.5f,10.5f);
		
		GradientPaint gradientPaint = new GradientPaint(x,y,color.darker(),x+width,y+height,color);
		g.setPaint(gradientPaint);

		g.fill(shape1);
		g.fill(shape2);
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2d =(Graphics2D)g;
		paintComponent(g2d);
	}
}
