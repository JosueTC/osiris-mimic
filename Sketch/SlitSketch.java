package gtc.AL.Inspector.Panels.OSIRIS.Mimic.Sketch;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;

import javax.swing.JLabel;


public class SlitSketch extends JLabel  {

	private Color color;
	
	
	public SlitSketch(Color color) {
		this.color=color;
	}

	public void paintComponent(Graphics2D g) {
		
		int x = (int)(this.getVisibleRect().getX()+this.getVisibleRect().getWidth()/2.5);
		int y = (int)(this.getVisibleRect().getY());
		int width = (int)(this.getVisibleRect().getWidth()/5.0);
	    int height = (int)(this.getVisibleRect().getHeight()/1.05);

		g.setColor(color);
		RoundRectangle2D.Float shape = new RoundRectangle2D.Float(x,y,width,height,10.5f,10.5f);
		Rectangle2D.Float shapeSlit = new Rectangle2D.Float(x,y+height * 0.5f-width/2,width,width);

		Area areaShape = new Area(shape);
		Area areaShapeSlit = new Area(shapeSlit);
		areaShape.subtract(areaShapeSlit);

		GradientPaint gradientPaint = new GradientPaint(x,y,color.darker(),x+width,y+height,color);
		g.setPaint(gradientPaint);
		g.fill(areaShape);
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2d =(Graphics2D)g;
		paintComponent(g2d);
	}
}

