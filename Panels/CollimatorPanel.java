package gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels;

import CANOPENNMOTORSCONTROLLER.CANOpenNMotorsController_ifce;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.ComponentsOsirisMimicId;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.CheckConfiguration.OpticalConflict;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.Sketch.CollimatorSketch;
import gtc.DSL.CK.ComponentModel.ComponentState;
import gtc.DSL.DAF.CORBAServices;
import gtc.DSL.DAF.Services.EnumConsumer;
import gtc.DSL.DAF.Services.EnumViewer;
import gtc.DSL.GUI_Kit.GUIBasics.BasicsGUIDefaults;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.geom.Point2D;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.BorderFactory;

import COLLIMATORCOMPONENT.colStat;
import DGT.GCSException;
import org.omg.CORBA.COMM_FAILURE;
import org.apache.log4j.Logger;
/**
* Represents the Collimator
* 
* @author mhuertas
*
*/
public class CollimatorPanel extends StateComponent  implements  EnumViewer {

	final static Logger log = Logger.getLogger(CollimatorPanel.class);
	
	private Point2D connectionPoint;
	private CollimatorSketch collimatorSketch;
	private colStat stateCollimator_;
	private EnumConsumer stateCollimatorConsumer_;
	private Timer timer;
	
	private static double ROTATOR_COMPENSATION_THRESHOLD = 10.0; // TODO 5239 - Rotator thresold para CG, ¿mismo valor?
	private static int COMPENSATION_CHECK_PERIOD=5000;
	/**
	*	Indicate if the compensation have taken place.
	*/
	private boolean isCompensationExecuted_;
	private double lastCompensatedRotatorPosition_;
	private double actualRotatorPosition_;
	private boolean isCompensationOK_;
	
	public CollimatorPanel()
	{
		super(ComponentsOsirisMimicId.COLLIMATORCOMPONENT);
		this.setOpaque(false);
		this.stateCollimatorConsumer_= new EnumConsumer(ComponentsOsirisMimicId.COLLIMATORCOMPONENT,"colStatus");
		this.stateCollimatorConsumer_.setViewer(this);
		this.collimatorSketch = new CollimatorSketch(Color.WHITE);
		this.collimatorSketch.setBorder(BorderFactory.createEmptyBorder(10,10, 10, 10));
		this.setLayout(new BorderLayout(10,10));
		this.setBackground(BasicsGUIDefaults.DARKER_GRAY);
		this.addComponentListener(this);
		this.add(collimatorSketch,BorderLayout.CENTER);
		
		
		
		isCompensationExecuted_ = true;
		isCompensationOK_= true;


		lastCompensatedRotatorPosition_=0;
		actualRotatorPosition_=0;
		

		
		Timer timer = new Timer();
        	timer.scheduleAtFixedRate(new CheckCompensationTask(),0, COMPENSATION_CHECK_PERIOD);
		
		
	}	

	
	
	@Override
	public Point2D getConnectionPoint() {
		return connectionPoint;
	}

	@Override
	public void propertyChange(int id, Object oldValue, Object newValue) {
	}

	@Override
	public void receiveMagnitudeChange(String component, String magnitude,
			int newState) {

		try 
		{

				
			this.stateCollimator_= COLLIMATORCOMPONENT.colStat.from_int(newState);
		
			//The compensation have taken place
			isCompensationExecuted_ =true;
		
			selectBorder();
		} 
		catch(Exception e)
		{
			e.printStackTrace();
		}

	}
	

	public Status getStatus() {
		if (!isCompensationOK_) 
			return Status.ERROR;
		if ((stateCollimator_ == colStat.HOMING || 
				stateCollimator_ == colStat.INITIALIZING || 
				stateCollimator_ == colStat.MOVING || 
				stateCollimator_ == colStat.STOPING))
			return Status.MOVING;
		if (stateCollimator_  == colStat.UNKNOWN )
			return Status.ERROR;
		return Status.READY;
	}

	
	public void updatePosition(double x, double y, double width,double height)
	{
		connectionPoint= new Point2D.Double(x+width*0.5,y+height*0.5);
	}
	
	
	class CheckCompensationTask extends TimerTask {
	        public void run() {
			try {
				 // TODO 5239 - Cambiar por Rotator CG
			     //org.omg.CORBA.Object obj1 = CORBAServices.resolve(ComponentsOsirisMimicId.ROTATORB);
			     //MACSPMACAXIS.MACSPMACAxis_ifce rotatorB = MACSPMACAXIS.MACSPMACAxis_ifceHelper.narrow(obj1);
				org.omg.CORBA.Object obj1 = CORBAServices.resolve(ComponentsOsirisMimicId.ROTATORCG);
				// TODO ¿Cuál es la clase de ésto? Revisar: /work/gcsop/etc/profiles/CG/Rotator.profile
				// ¿CGROTATOR?
				CANOPENNMOTORSCONTROLLER.CANOpenNMotorsController_ifce rotatorCG = CANOPENNMOTORSCONTROLLER.CANOpenNMotorsController_ifceHelper.narrow(obj1);
				// TODO 5239 - Cambiar por Rotator CG



			     actualRotatorPosition_ = rotatorCG.position(); // TODO 5239 - Cambiar por Rotator CG

			     if (isCompensationExecuted_)
			     {
				     isCompensationOK_ = true;
				     isCompensationExecuted_ = false;
				     lastCompensatedRotatorPosition_ = actualRotatorPosition_;
			     }
			     else
			     {
			     
			     	double distanceWithoutCompensation = Math.abs(actualRotatorPosition_-lastCompensatedRotatorPosition_);
				if (distanceWithoutCompensation > ROTATOR_COMPENSATION_THRESHOLD)
				{
					isCompensationOK_ = false;
				}

			     }
			     
			     org.omg.CORBA.Object obj2 = CORBAServices.resolve(ComponentsOsirisMimicId.OSIRIS);
			     DCF.Device_ifce osiris = DCF.Device_ifceHelper.narrow(obj2);
			     
			     
			     
			     ComponentState state = ComponentState.valueOf(osiris.state().scoped_name);
			     
			     switch(state) {
				case FAULT:
				case OFF:
				case INACTIVE:
				case DISABLED:
				case ON:
					isCompensationOK_= false;
					break;
			     }
			     
			 }
			catch (COMM_FAILURE e) {
				// TODO Cass - 5239: Rotator CG
				log.error("It is not possible to communicate with: " + ComponentsOsirisMimicId.ROTATORCG + " or "+ ComponentsOsirisMimicId.OSIRIS + ", please, check these devices" );
				isCompensationOK_ = false;
			}
			catch (Exception e ) {
			 	e.printStackTrace();
				isCompensationOK_ = false;				
			 }
			 
			 selectBorder();
		
	        }
    	}
}



