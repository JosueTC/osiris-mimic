package gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels;

import gtc.AL.Inspector.Panels.OSIRIS.Mimic.CheckConfiguration.BaseConflict.CONFLICTTYPE;

import java.util.List;

public interface BadConfigurationListener {
	
	public void notifyBadConfiguration(	List<MarkAsBadConfiguration> inConflict,
											List<MarkAsBadConfiguration> noConflict ,
											CONFLICTTYPE type);

}
