package gtc.AL.Inspector.Panels.OSIRIS.Mimic;

import java.awt.Font;

public class GUIConstants {
	
	public static  int FONT_SIZE = 25;

	public static  int FONT_SIZE_HUGE = 100;
	public static  int FONT_SIZE_BIG = 120;
	public static  int FONT_SIZE_NORMAL = 50;
	public static  int FONT_SIZE_MEDIUM = 25;
	public static  int FONT_SIZE_SMALL = 20;	
	

	public static  Font FONT_HUGE = new Font("Courier", Font.PLAIN, FONT_SIZE_HUGE);
	public static  Font FONT_ = new Font("Courier", Font.PLAIN, FONT_SIZE);
	public static  Font FONT_SMALL = new Font("Courier", Font.PLAIN, FONT_SIZE_SMALL);
	public static  Font FONT_MEDIUM = new Font("Courier", Font.PLAIN, FONT_SIZE_MEDIUM);
	public static  Font FONT_NORMAL = new Font("Courier", Font.PLAIN, FONT_SIZE_NORMAL);
	public static  Font FONT_BIG = new Font("Courier", Font.PLAIN, FONT_SIZE_BIG);
	
}
