package gtc.AL.Inspector.Panels.OSIRIS.Mimic.Sketch;

import gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.MarkAsBadConfiguration;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.PathAbled;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.StateComponent;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import java.util.Iterator;
import java.util.Vector;

public class RayLightSketch implements MarkAsBadConfiguration {
	
	private Color color;
	private Color defaultColor;
	
	private Vector<PathAbled> pathAbleds;
	
	public RayLightSketch(Vector<PathAbled> pathAbleds,Color defaultColor) {
		super();
		this.pathAbleds = pathAbleds;
		this.defaultColor=defaultColor;
		this.color=defaultColor;
	}

	public void draw(Graphics2D g) {
		int type = AlphaComposite.SRC_OVER;
		AlphaComposite alphaComposite = AlphaComposite.getInstance(type, 0.7f);
		
		
		Iterator<PathAbled> iter =  pathAbleds.iterator();
		Point2D point1 = iter.next().getConnectionPoint();
		
		GeneralPath polyline = 
			new GeneralPath(GeneralPath.WIND_EVEN_ODD, pathAbleds.size());

		polyline.moveTo (point1.getX(), point1.getY());

		while (iter.hasNext())
		{
			
			Point2D point2 = iter.next().getConnectionPoint();
			polyline.lineTo(point2.getX(), point2.getY());
		}
		
		g.setComposite(alphaComposite);
		g.setStroke(new BasicStroke(25.0f,BasicStroke.CAP_ROUND,BasicStroke.JOIN_ROUND));
		g.setColor(Color.GRAY);
		g.draw(polyline);
		
		g.setStroke(new BasicStroke(18.0f));
		g.setColor(color);
		g.setComposite(alphaComposite);
		g.draw(polyline);
		
		
		g.setComposite(AlphaComposite.getInstance(type,1f));

	}
	
	public void setColor(Color color) {
		this.color=color;
	}
	
	public void resetColor() {
		this.color=defaultColor;
	}

	@Override
	public void setBadConfiguration(boolean badConfiguration) {
		if(badConfiguration) {
			this.setColor(Color.RED);
		} else {
			resetColor();
		}
	}
}
