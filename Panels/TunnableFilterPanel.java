package gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels;

import gtc.AL.Inspector.Panels.OSIRIS.Mimic.ComponentsOsirisMimicId;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.Sketch.TunnableFilterSketch;
import gtc.DSL.DAF.Services.EnumConsumer;
import gtc.DSL.DAF.Services.EnumViewer;
import gtc.DSL.GUI_Kit.GUIBasics.BasicsGUIDefaults;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.geom.Point2D;

import WHEELCOMPONENT.wheelStat;


/**
 * Represents the Turnable Filer. 
 * 
 * @author mhuertas
 *
 */
public class TunnableFilterPanel extends StateComponent  implements EnumViewer{
	
	private static final long serialVersionUID = 1L;
	private wheelStat stateWheel_;
	private EnumConsumer stateWheelConsumer_;
	private TunnableFilterSketch tunnableFilterSketch_;
	private TunnableFilterTelemetryPanel tunnableFilterTelemetryPanel_;
	private Point2D connectionPoint;
	
	public TunnableFilterPanel(String component, String horientation) {
		super(component);
		this.setOpaque(false);
		this.stateWheelConsumer_= new EnumConsumer(ComponentsOsirisMimicId.WHEELCOMPONENT_4,"wheelStatus");
		this.stateWheelConsumer_.setViewer(this);
		this.setLayout(new BorderLayout(1,1));
		this.setBackground(BasicsGUIDefaults.DARKER_GRAY);
		this.tunnableFilterSketch_ = new TunnableFilterSketch(Color.WHITE);
		this.tunnableFilterTelemetryPanel_=new TunnableFilterTelemetryPanel(component);
		add(tunnableFilterTelemetryPanel_,horientation);
		add(tunnableFilterSketch_,BorderLayout.EAST);
		
		this.addComponentListener(this);
	}	
	

	public Status getStatus() {
		if (stateWheel_ == wheelStat.UNKNOWN )
			return Status.ERROR;
		if ((stateWheel_ == wheelStat.HOMING || 
				stateWheel_ == wheelStat.INITIALIZING || 
				stateWheel_ == wheelStat.MOVING || 
				stateWheel_ == wheelStat.STOPING))
			return Status.MOVING;
		
		return Status.READY;
	}


	@Override
	public Point2D getConnectionPoint() {
		return connectionPoint;
	}
	


	public void updatePosition(double x, double y, double width,double height)
	{
		this.tunnableFilterSketch_.setPreferredSize(new Dimension((int)(width*0.3),tunnableFilterSketch_.getHeight()));
		
		connectionPoint =  new Point2D.Double(x+width/3.5,y+height/2.0);
	}


	@Override
	public void receiveMagnitudeChange(String component, String magnitude,
			int newState) {
		try {
			this.stateWheel_= WHEELCOMPONENT.wheelStat.from_int(newState);
			selectBorder();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
