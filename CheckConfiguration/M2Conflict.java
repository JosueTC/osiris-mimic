package gtc.AL.Inspector.Panels.OSIRIS.Mimic.CheckConfiguration;

import gtc.AL.Inspector.Panels.OSIRIS.Mimic.ComponentsOsirisMimicId;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.MarkAsBadConfiguration;
import gtc.DSL.DAF.Services.EnumConsumer;
import gtc.DSL.DAF.Services.EnumViewer;

import java.util.ArrayList;
import java.util.List;

import M2EXTERNALBAFFLE.PositionFlaps_t;

public class M2Conflict extends BaseConflict {
	
	private MarkAsBadConfiguration component_;
	private enum MONITORENUMS {M2InnerBaffle,
								M2ExternalBaffle};
	
	public M2Conflict(MarkAsBadConfiguration component_,CONFLICTTYPE type) {
		super(type);
		this.component_ = component_;
		createMonitors();
	}

	class MonitorsEnumConsumer implements EnumViewer {
		private MONITORENUMS monitor_;
		
		public MonitorsEnumConsumer(MONITORENUMS monitor) {	
			this.monitor_=monitor;
		}
		
		@Override
		public void receiveMagnitudeChange(String component, String magnitude,int newState) {
			
			List<MarkAsBadConfiguration> inConflict = new ArrayList<MarkAsBadConfiguration>();
			List<MarkAsBadConfiguration> noConflict = new ArrayList<MarkAsBadConfiguration>();

			PositionFlaps_t PositionFlaps = M2EXTERNALBAFFLE.PositionFlaps_t.from_int(newState);
			
			
			try {
				switch (monitor_) {
					case M2InnerBaffle:
						if (PositionFlaps == M2EXTERNALBAFFLE.PositionFlaps_t.DEPLOYED) {
							noConflict.add(component_);
						} else {
							inConflict.add(component_);
						}
						break;
					case M2ExternalBaffle:
			
						if ( PositionFlaps == M2EXTERNALBAFFLE.PositionFlaps_t.DEPLOYED) {
							noConflict.add(component_);
						} else {
							inConflict.add(component_);
						}
						break;
				}
				notifyBadConfigurationListener(inConflict,noConflict,type);
				
			} catch (Exception e ) {
			}
		}
	
		@Override
		public void propertyChange(int id, java.lang.Object oldValue,java.lang.Object newValue) {
		}
	}
	
	private void createMonitors() {
		
		EnumConsumer M2InnerBaffle_ = new EnumConsumer(ComponentsOsirisMimicId.M2ExternalBaffle,"positionInternalFlaps");
		M2InnerBaffle_.setViewer(new MonitorsEnumConsumer(MONITORENUMS.M2InnerBaffle));
		
		EnumConsumer M2ExternalBaffle_ = new EnumConsumer(ComponentsOsirisMimicId.M2ExternalBaffle,"positionExternalFlaps");
		M2InnerBaffle_.setViewer(new MonitorsEnumConsumer(MONITORENUMS.M2ExternalBaffle));

	}
	

}
