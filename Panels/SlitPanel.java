package gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels;

import gtc.AL.Inspector.Panels.OSIRIS.Mimic.ComponentsOsirisMimicId;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.GUIConstants;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.Sketch.SlitSketch;
import gtc.DSL.DAF.Services.DataConsumer;
import gtc.DSL.DAF.Services.DataViewer;
import gtc.DSL.DAF.Services.EnumConsumer;
import gtc.DSL.DAF.Services.EnumViewer;
import gtc.DSL.GUI_Kit.GUIBasics.BasicsGUIDefaults;
import gtc.DSL.GUI_Kit.GUIBasics.Capsules.LabelCapsule;
import gtc.DSL.Types.IntPair;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.geom.Point2D;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

import DGT.DoubleArray;
import SLITSCOMPONENT.SlitsStat;


/**
 * Represents a Slit wheel 
 * 
 * @author mhuertas
 *
 */
public class SlitPanel extends StateComponent implements   EnumViewer, DataViewer{

	private enum MONITORBOOLEAN {isMaskSelPositiveLimitSet,
		isMaskSelNegativeLimitSet,
		isMaskSelFatalFollowingError,
		isMaskLoadPositiveLimitSet,
		isMaskLoadNegativeLimitSet,
		isMaskLoadFatalFollowingError};

	private Point2D connectionPoint;
	private SlitSketch slitSketch;
	private SlitsStat stateSlits_;
	private EnumConsumer stateSlitsConsumer_;
	private Object[] opticalElementTable;
	private DataConsumer currentPositionConsumer_;
	private int currentPosition_=-1;
	private LabelCapsule positionName;
	
	private EnumConsumer isMaskSelPositiveLimitSetConsumer_;
	private EnumConsumer isMaskSelNegativeLimitSetConsumer_;
	private EnumConsumer isMaskSelFatalFollowingErrorConsumer_;
	private EnumConsumer isMaskLoadPositiveLimitSetConsumer_;
	private EnumConsumer isMaskLoadNegativeLimitSetConsumer_;
	private EnumConsumer isMaskLoadFatalFollowingErrorConsumer_;
	

	private boolean isMaskSelPositiveLimitSet;
	private boolean isMaskSelNegativeLimitSet;
	private boolean isMaskSelFatalFollowingError;
	private boolean isMaskLoadPositiveLimitSet;
	private boolean isMaskLoadNegativeLimitSet;
	private boolean isMaskLoadFatalFollowingError;
	

	public SlitPanel(Object []opticalElementTable)
	{
		super(ComponentsOsirisMimicId.SLITSCOMPONENT_1);
		
		this.setOpaque(false);
		
		this.stateSlitsConsumer_= new EnumConsumer(ComponentsOsirisMimicId.SLITSCOMPONENT_1,"slitsStatus");
		this.stateSlitsConsumer_.setViewer(this);
		this.currentPositionConsumer_= new DataConsumer(ComponentsOsirisMimicId.SLITSCOMPONENT_1,"currentPosition");
		this.currentPositionConsumer_.setViewer(this);
		
		
		this.isMaskSelPositiveLimitSetConsumer_ = new EnumConsumer(ComponentsOsirisMimicId.SLITSCOMPONENT_1, "isMaskSelPositiveLimitSet");
		this.isMaskSelNegativeLimitSetConsumer_ = new EnumConsumer(ComponentsOsirisMimicId.SLITSCOMPONENT_1, "isMaskSelNegativeLimitSet");
		this.isMaskSelFatalFollowingErrorConsumer_ = new EnumConsumer(ComponentsOsirisMimicId.SLITSCOMPONENT_1, "isMaskSelFatalFollowingError");
		this.isMaskLoadPositiveLimitSetConsumer_ = new EnumConsumer(ComponentsOsirisMimicId.SLITSCOMPONENT_1, "isMaskLoadPositiveLimitSet");
		this.isMaskLoadNegativeLimitSetConsumer_ = new EnumConsumer(ComponentsOsirisMimicId.SLITSCOMPONENT_1, "isMaskLoadNegativeLimitSet");
		this.isMaskLoadFatalFollowingErrorConsumer_ = new EnumConsumer(ComponentsOsirisMimicId.SLITSCOMPONENT_1, "isMaskLoadFatalFollowingError");
		

		
		this.isMaskSelPositiveLimitSetConsumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.isMaskSelPositiveLimitSet));
		this.isMaskSelNegativeLimitSetConsumer_ .setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.isMaskSelNegativeLimitSet));
		this.isMaskSelFatalFollowingErrorConsumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.isMaskSelFatalFollowingError));
		this.isMaskLoadPositiveLimitSetConsumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.isMaskLoadPositiveLimitSet));
		this.isMaskLoadNegativeLimitSetConsumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.isMaskLoadNegativeLimitSet));
		this.isMaskLoadFatalFollowingErrorConsumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.isMaskLoadFatalFollowingError));
		this.slitSketch = new SlitSketch(Color.WHITE);
		this.opticalElementTable=opticalElementTable;
		
		
		this.setLayout(new BorderLayout(1,1));
		this.setBackground(BasicsGUIDefaults.DARKER_GRAY);
		
		
		this.positionName = new LabelCapsule("N/A");
		this.positionName.setFont(GUIConstants.FONT_);
		this.positionName.setPreferredSize(new Dimension(60,30));
		this.positionName.setBorder(BorderFactory.createEmptyBorder(2,2, 2, 2));
		
		
		JPanel northPanel = new JPanel();
		northPanel.setBackground(BasicsGUIDefaults.TRANSPARENT);
		northPanel.setLayout(new BoxLayout(northPanel,BoxLayout.X_AXIS));
		northPanel.add(this.positionName);
		
		add(northPanel,BorderLayout.NORTH);
		add(slitSketch,BorderLayout.CENTER);
		this.addComponentListener(this);
	}
	
	
	public String getPositionName()
	{
		try
		{
			return  (String)opticalElementTable[currentPosition_];
		} catch(ArrayIndexOutOfBoundsException e)
		{
			return "N/A";
		}
	}

	@Override
	public Point2D getConnectionPoint() {
		return connectionPoint;
	}


	@Override
	public void propertyChange(int id, Object oldValue, Object newValue) {
	}

	@Override
	public void receiveMagnitudeChange(String component, String magnitude,
			int newState) {
		this.stateSlits_= SLITSCOMPONENT.SlitsStat.from_int(newState);
		selectBorder();
	}
	


	@Override
	public void receiveData(double sample, long timeStamp) {
	
		if (sample!=0)
		{
			currentPosition_=(int)(sample-1)*2;
			String position= getPositionName();
			positionName.setLabel(position);
		} 
		else
		{
			positionName.setLabel("");
			currentPosition_=-1;
		}
		
		if (currentPosition_==-1)
			slitSketch.setVisible(false);
		else
			slitSketch.setVisible(true);
	}

	@Override
	public void receiveData(DoubleArray sample, long timeStamp) {
	}


	@Override
	public void receiveData(DoubleArray sample,long index,IntPair pair) {
	}	


	
	class MonitorsBooleanConsumer implements EnumViewer
	{
		private MONITORBOOLEAN monitor_;
		
		private String [] isMaskSelPositiveLimitSetLabels;
		private String [] isMaskSelNegativeLimitSetLabels;
		private String [] isMaskSelFatalFollowingErrorLabels;
		private String [] isMaskLoadPositiveLimitSetLabels;
		private String [] isMaskLoadNegativeLimitSetLabels;
		private String [] isMaskLoadFatalFollowingErrorLabels;
		
		
		public MonitorsBooleanConsumer(MONITORBOOLEAN monitor)
		{	
			this.monitor_=monitor;
		}
		@Override
		public void receiveMagnitudeChange(String component, String magnitude,
				int newState) {

			try {
				switch (monitor_)
				{
					case isMaskSelPositiveLimitSet:
						isMaskSelPositiveLimitSet = new Boolean( isMaskSelPositiveLimitSetLabels[newState] );
						break;
					case isMaskSelNegativeLimitSet:
						isMaskSelNegativeLimitSet = new Boolean( isMaskSelNegativeLimitSetLabels[newState] );
						break;
					case isMaskSelFatalFollowingError:
						isMaskSelFatalFollowingError = new Boolean( isMaskSelFatalFollowingErrorLabels[newState] );	 		
						break;
					case isMaskLoadPositiveLimitSet:
						isMaskLoadPositiveLimitSet = new Boolean( isMaskLoadPositiveLimitSetLabels[newState] );
						break;
					case isMaskLoadNegativeLimitSet:
						isMaskLoadNegativeLimitSet = new Boolean( isMaskLoadNegativeLimitSetLabels[newState] );
						break;
					case isMaskLoadFatalFollowingError:
						isMaskLoadFatalFollowingError = new Boolean( isMaskLoadFatalFollowingErrorLabels[newState] );
						break;
				}
				selectBorder();
			} catch (Exception e) {
				e.printStackTrace();
			}


		}

		@Override
		public void propertyChange(int id, Object oldValue, Object newValue) {
			switch (monitor_)
			{
				case isMaskSelPositiveLimitSet:
					isMaskSelPositiveLimitSetLabels=(String[])newValue;
					break;
				case isMaskSelNegativeLimitSet:
					isMaskSelNegativeLimitSetLabels=(String[])newValue;
					break;
				case isMaskSelFatalFollowingError:
					isMaskSelFatalFollowingErrorLabels=(String[])newValue;
					break;
				case isMaskLoadPositiveLimitSet:		
					isMaskLoadPositiveLimitSetLabels=(String[])newValue;
					break;
				case isMaskLoadNegativeLimitSet:
					isMaskLoadNegativeLimitSetLabels=(String[])newValue;
					break;
				case isMaskLoadFatalFollowingError:
					isMaskLoadFatalFollowingErrorLabels=(String[])newValue;
					break;
			}
		}
	}

	public Status getStatus() {
	
	
		if (stateSlits_ == SlitsStat.UNKNOWN ) {
			return Status.ERROR;
		}
			
		if (isMaskSelPositiveLimitSet || isMaskSelNegativeLimitSet || isMaskSelFatalFollowingError
				|| isMaskLoadPositiveLimitSet || isMaskLoadNegativeLimitSet || isMaskLoadFatalFollowingError) {
			return Status.ERROR;
		}
		
		if ((stateSlits_ == SlitsStat.HOMING || 
				stateSlits_ == SlitsStat.MOVING || 
				stateSlits_ == SlitsStat.STOPING))
			return Status.MOVING;
		return Status.READY;
	}
	
	public void updatePosition(double x, double y, double width,double height)
	{
		connectionPoint= new Point2D.Double(x+width/2.5+width/10.0,y+height*0.5);
	    this.positionName.setMaximumSize(new Dimension((int)(width),(int)(height*0.2)));
	    this.positionName.setPreferredSize(new Dimension((int)(width),(int)(height*0.2)));

	}
}
