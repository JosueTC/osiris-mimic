package gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.geom.RoundRectangle2D;

import javax.swing.border.AbstractBorder;

public class RoundedBorder extends AbstractBorder {
	private Color color;
	
	@Override
	public Insets getBorderInsets(Component c, Insets insets) {
		return new Insets ( 5,5,5,5) ;
	}

	@Override
	public Insets getBorderInsets(Component c) {
		return new Insets ( 5,5,5,5) ;
	}
	
	public RoundedBorder(Color color)
	{
		this.color=color;
	}
	
	public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
		Graphics2D g2d =(Graphics2D)g;
		
		g2d.setColor ( color ) ;
		g2d.setStroke(new BasicStroke(15.0f));
		RoundRectangle2D.Double shape = new RoundRectangle2D.Double( x , y , width , height ,15.0, 15.0);
		
		g2d.fill(shape);

	}

}	

