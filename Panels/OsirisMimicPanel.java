package gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels;

import static gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.GUIPositions.ALIGN_FILTERS_Y;
import static gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.GUIPositions.ALIGN_FILTERS_Y_OFFSET;
import static gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.GUIPositions.ALIGN_ICMMIRROR_X;
import static gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.GUIPositions.ALIGN_ICM_MIRROR_Y;
import static gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.GUIPositions.ALIGN_SLIT_X;
import static gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.GUIPositions.SIZE_FILTER1_X;
import static gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.GUIPositions.SIZE_FILTER1_Y;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.ComponentsOsirisMimicId;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.CheckConfiguration.BaseConflict;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.CheckConfiguration.BaseConflict.CONFLICTTYPE;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.CheckConfiguration.ICMMirrorLampsConflict;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.CheckConfiguration.M2Conflict;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.CheckConfiguration.M3Conflict;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.CheckConfiguration.OpticalConflict;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.Sketch.RayLightSketch;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.Sketch.StarSketch;
import gtc.DSL.CK.ComponentModel.ComponentConfigurations;
import gtc.DSL.DAF.Services.DataConsumer;
import gtc.DSL.DAF.Services.DataViewer;
import gtc.DSL.DAF.Services.EnumConsumer;
import gtc.DSL.DAF.Services.EnumViewer;
import gtc.DSL.GUI_Kit.GUIBasics.BasicsGUIDefaults;
import gtc.DSL.GUI_Kit.ScalarMonitors.ScalarMonitorCapsule;
import gtc.DSL.Types.IntPair;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

import javax.swing.JPanel;

import DGT.DoubleArray;
import ICMSPECTRAL.MirrorStatus_t;

public class OsirisMimicPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	final static Logger log = Logger.getLogger(OsirisMimicPanel.class);

	private Map<String,StateComponent> components_;
	
	private RenderingHints renderingHints;
	
	private Object[] filter1ElementTable;
	private Object[] filter2ElementTable;
	private Object[] filter3ElementTable;
	private Object[] grismElementTable;

	private StarSketch starSketch;
	private RayLightSketch rayLightSketchStarCurrent_;
	private RayLightSketch rayLightSketchICM;
	private RayLightSketch rayLightSketchStar;
	
	private ScalarMonitorCapsule offsetOsiris_ = new ScalarMonitorCapsule();
	private StatusPanel statusCapsule;
	
	private int grismPosition_;
	
	
	private enum MONITORSCALAR {currentPosition,
		currentFocus};
		
	private enum MONITORENUMS {icmspectral};

	public OsirisMimicPanel() 
	{
		setLayout(null);
		this.setOpaque(false);

		this.components_ = new  HashMap<String,StateComponent>();
		
		buildGUI();
		
		renderingHints = new RenderingHints(
				RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		
		// TODO Cass - 5239: Referencia al ICM de Nasmyth B
		BaseConflict icmLampConflict = new ICMMirrorLampsConflict(components_,CONFLICTTYPE.ICM);
		icmLampConflict.addBadConfigurationListener(new BadConfigurationListener() {
			@Override
			public void notifyBadConfiguration(List<MarkAsBadConfiguration> inConflict,
					List<MarkAsBadConfiguration> noConflict,CONFLICTTYPE type) {
				for (MarkAsBadConfiguration component : noConflict )
					component.setBadConfiguration(false);
				
				for (MarkAsBadConfiguration component : inConflict)
					component.setBadConfiguration(true);
				
			}
		});
		icmLampConflict.addBadConfigurationListener(statusCapsule);
		
		BaseConflict opticalConflict = new OpticalConflict(components_,
																filter1ElementTable,
																filter2ElementTable,
																filter3ElementTable,
																grismElementTable,
																CONFLICTTYPE.OPTIC);
		opticalConflict.addBadConfigurationListener(new BadConfigurationListener() {
			@Override
			public void notifyBadConfiguration(List<MarkAsBadConfiguration> inConflict,
					List<MarkAsBadConfiguration> noConflict,CONFLICTTYPE type) {
				for (MarkAsBadConfiguration component : noConflict )
					component.setBadConfiguration(false);
				
				for (MarkAsBadConfiguration component : inConflict)
					component.setBadConfiguration(true);
			}
		});
		opticalConflict.addBadConfigurationListener(statusCapsule);
		
		BaseConflict  m2Conflict = new M2Conflict(rayLightSketchStarCurrent_,CONFLICTTYPE.M2);
		m2Conflict.addBadConfigurationListener(new BadConfigurationListener() {
			@Override
			public void notifyBadConfiguration(List<MarkAsBadConfiguration> inConflict,
					List<MarkAsBadConfiguration> noConflict,CONFLICTTYPE type) {
				for (MarkAsBadConfiguration component : noConflict )
					component.setBadConfiguration(false);
				for (MarkAsBadConfiguration component : inConflict)
					component.setBadConfiguration(true);
			}
		});
		m2Conflict.addBadConfigurationListener(statusCapsule);
		
		// TODO Cass - 5239: M3 Conflicto. Hacer pruebas con ésto
		//BaseConflict  m3Conflict = new M3Conflict(rayLightSketchStarCurrent_,CONFLICTTYPE.M3); // M3 TOWER
		BaseConflict  m3Conflict = new M3Conflict(rayLightSketchStarCurrent_,CONFLICTTYPE.M3PARK); // M3 PARK
		m3Conflict.addBadConfigurationListener(new BadConfigurationListener() {
			@Override
			public void notifyBadConfiguration(List<MarkAsBadConfiguration> inConflict,
					List<MarkAsBadConfiguration> noConflict,CONFLICTTYPE type) {
				for (MarkAsBadConfiguration component : noConflict )
					component.setBadConfiguration(false);
				
				for (MarkAsBadConfiguration component : inConflict)
					component.setBadConfiguration(true);
			}
		});
		m3Conflict.addBadConfigurationListener(statusCapsule);
		
		createMonitors();
	}
	
	class MonitorsEnumConsumer implements EnumViewer {
		private MONITORENUMS monitor_;
		public MonitorsEnumConsumer(MONITORENUMS monitor) {	
			this.monitor_=monitor;
		}
		
		@Override
		public void receiveMagnitudeChange(String component, String magnitude,int newState) {
			try {
				switch (monitor_) {
					case icmspectral:
					     ICMSpectralMirrorPanel iCMSpectralMirror = (ICMSpectralMirrorPanel)components_.get(ComponentsIDs.ICMSPECTRALMIRROR);

					     MirrorStatus_t mirrorStatus = ICMSPECTRAL.MirrorStatus_t.from_int(newState);
					     if (mirrorStatus == ICMSPECTRAL.MirrorStatus_t.WORK) {
						     if (iCMSpectralMirror != null)
						     iCMSpectralMirror.setLocation(ALIGN_ICMMIRROR_X,ALIGN_ICM_MIRROR_Y);
						     rayLightSketchStarCurrent_=rayLightSketchICM;
						     starSketch.setVisible(false);
					     }
					     if (mirrorStatus == ICMSPECTRAL.MirrorStatus_t.PARK) {

						     if (iCMSpectralMirror != null)
							     iCMSpectralMirror.setLocation(ALIGN_ICMMIRROR_X,ALIGN_FILTERS_Y);
						     rayLightSketchStarCurrent_=rayLightSketchStar;
						     starSketch.setVisible(true);
					     }
					     repaint();
					     break;
				}
			} catch (Exception e ) {
				e.printStackTrace();
			}
		}
		@Override
		public void propertyChange(int id, java.lang.Object oldValue,java.lang.Object newValue) {
		}
	}

	
	class MonitorsScalarConsumer implements DataViewer {
		private MONITORSCALAR monitor_;
		public MonitorsScalarConsumer(MONITORSCALAR monitor) {	
			this.monitor_=monitor;
			receiveData(0,0); //TODO REMOVE THIS
		}

		@Override
		public void receiveData(double sample, long timeStamp) {
			try {
				switch (monitor_) {
					case currentPosition:
						grismPosition_=(int)sample;
						
						if (grismPosition_ <= 0 || grismPosition_ > grismElementTable.length) {
							if (grismPosition_ != 0)
								log.error("Index out of bounds when was tryging to access to grismElementTable");
							else
								log.debug("The grismPostion has the UNKNOWN state, due to its value is: 0");
							
							return;
						}

						StateComponent tf1 = components_.get(ComponentsIDs.TUNABLEFILTERSCOMPONENT1);
						StateComponent tf2 = components_.get(ComponentsIDs.TUNABLEFILTERSCOMPONENT2);
						StateComponent filter = components_.get(ComponentsIDs.WHEELCOMPONENT4);

						String positionName=(String)grismElementTable[grismPosition_-1];
						if(positionName.indexOf("TF_RED")!=-1) {
						
							if (!tf1.isVisible()) tf1.setVisible(true);
							if (tf2.isVisible()) tf2.setVisible(false);
							if (filter.isVisible())  filter.setVisible(false);
						}else if(positionName.indexOf("TF_BLUE")!=-1) {
						
							if (tf1.isVisible()) tf1.setVisible(false);
							if (!tf2.isVisible()) tf2.setVisible(true);
							if (filter.isVisible()) filter.setVisible(false);
						} else 
						{
							if (tf1.isVisible()) tf1.setVisible(false);
							if (tf2.isVisible()) tf2.setVisible(false);
							if (!filter.isVisible()) filter.setVisible(true);
						}
						repaint();
						break;
					case currentFocus:
						if (Math.abs(sample + 290000) <10) {
							offsetOsiris_.setBorder(new RoundedBorder(BasicsGUIDefaults.DARKER_GRAY));
						
						} else {
							offsetOsiris_.setBorder(new RoundedBorder(StateComponent.TRANSLUCID_RED));
						}
						repaint();
						break;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		@Override
		public void receiveData(DoubleArray sample, long timeStamp) {
		}

		@Override
		public void receiveData(DoubleArray sample,long index,IntPair pair) {
		}	

		@Override
		public void propertyChange(int id, Object oldValue, Object newValue) {
		}
	}
	
	public void paint(Graphics g) {
		Graphics2D g2d = (Graphics2D)g;
		
		g2d.setRenderingHints(renderingHints);
		g2d.setBackground(Color.BLACK);
		g2d.setColor(Color.BLACK);
		Dimension size = getSize();
		g2d.fillRect(0, 0, (int)size.getWidth(), (int)size.getHeight());
		super.paint(g2d);
		
		rayLightSketchStarCurrent_.draw(g2d);
	}

	
	private void createMonitors() {
		EnumConsumer  icmMirrorStatusConsumer_ = new EnumConsumer(ComponentsOsirisMimicId.ICMSPECTRAL,"mirrorStatus");
		icmMirrorStatusConsumer_.setViewer(new MonitorsEnumConsumer(MONITORENUMS.icmspectral));
		
		DataConsumer grismPositionConsumer_  = new DataConsumer(ComponentsOsirisMimicId.WHEELCOMPONENT_4,"currentPosition");
		grismPositionConsumer_.setViewer(new MonitorsScalarConsumer(MONITORSCALAR.currentPosition));
		
		DataConsumer focusPositionConsumer_  = new DataConsumer(ComponentsOsirisMimicId.FOCUS,"currentPosition");
		focusPositionConsumer_.setViewer(new MonitorsScalarConsumer(MONITORSCALAR.currentFocus));
	}
	
	private void buildGUI() {
		GUIWidgetBuilder GUIWidgetBuilder = new GUIWidgetBuilder();
		
		
		this.starSketch = GUIWidgetBuilder.buildStar();
		
		JPanel ring = GUIWidgetBuilder.buildRing();
		JPanel cCDTelemetryPanel = GUIWidgetBuilder.buildCCDTelemetry();
		JPanel cCDWindowsPanel = GUIWidgetBuilder.buildCCDWindowTelemetry();
		statusCapsule = GUIWidgetBuilder.buildStatusCapsule();
		//ScalarMonitorCapsule ccdTemperature = GUIWidgetBuilder.buildTemperatureCapsule();
		//ScalarMonitorCapsule ccdPressure = GUIWidgetBuilder.buildPressureCapsule();
		TemperatureAndPressurePanel TemperatureAndPressurePanel = GUIWidgetBuilder.buildTemperatureAndPressurePanel();
		
		CCDStatus ccdStatus = GUIWidgetBuilder.buildCCDStatus();
		
		StateComponent icm = GUIWidgetBuilder.buildICM();
		StateComponent icmMirror = GUIWidgetBuilder.buildICMMirror();
		StateComponent slit = GUIWidgetBuilder.buildSlit();
		StateComponent collimator = GUIWidgetBuilder.buildCollimator();
		StateComponent camera = GUIWidgetBuilder.buildCamera();
		try {
		filter1ElementTable = (Object[])ComponentConfigurations.getPropertyValue(ComponentsOsirisMimicId.OSIRIS,"opticalIdentifierFilterWheel1");
		StateComponent filter1 = GUIWidgetBuilder.buildFilter(filter1ElementTable,ComponentsOsirisMimicId.WHEELCOMPONENT_1, 
																 SIZE_FILTER1_X,SIZE_FILTER1_Y, ALIGN_SLIT_X,ALIGN_FILTERS_Y);
		components_.put(ComponentsIDs.WHEELCOMPONENT1, filter1);
		add(filter1);
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
		filter2ElementTable = (Object[])ComponentConfigurations.getPropertyValue(ComponentsOsirisMimicId.OSIRIS,"opticalIdentifierFilterWheel2");
		StateComponent filter2 = GUIWidgetBuilder.buildFilter(filter2ElementTable,ComponentsOsirisMimicId.WHEELCOMPONENT_2, 
																SIZE_FILTER1_X,SIZE_FILTER1_Y, ALIGN_SLIT_X,ALIGN_FILTERS_Y+ALIGN_FILTERS_Y_OFFSET);
		components_.put(ComponentsIDs.WHEELCOMPONENT2, filter2);
		add(filter2);
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
		filter3ElementTable = (Object[])ComponentConfigurations.getPropertyValue(ComponentsOsirisMimicId.OSIRIS,"opticalIdentifierFilterWheel3");
		StateComponent filter3 = GUIWidgetBuilder.buildFilter(filter3ElementTable,ComponentsOsirisMimicId.WHEELCOMPONENT_3,
																SIZE_FILTER1_X,SIZE_FILTER1_Y, ALIGN_SLIT_X,ALIGN_FILTERS_Y+2*ALIGN_FILTERS_Y_OFFSET);
		components_.put(ComponentsIDs.WHEELCOMPONENT3, filter3);
		add(filter3);
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
		grismElementTable = (Object[])ComponentConfigurations.getPropertyValue(ComponentsOsirisMimicId.OSIRIS,"opticalIdentifierPupilWheel");
		StateComponent grism = GUIWidgetBuilder.buildGrism(grismElementTable,ComponentsOsirisMimicId.WHEELCOMPONENT_4,
																SIZE_FILTER1_X,SIZE_FILTER1_Y, ALIGN_SLIT_X,ALIGN_FILTERS_Y+3*ALIGN_FILTERS_Y_OFFSET);
		components_.put(ComponentsIDs.WHEELCOMPONENT4, grism);
		add(grism);
		} catch (Exception e) {
			e.printStackTrace();
		}

		
		StateComponent tunnableFilter1 = GUIWidgetBuilder.buildTunnableFilter(ComponentsOsirisMimicId.TUNNABLEFILTER1, SIZE_FILTER1_X,200,ALIGN_SLIT_X,ALIGN_FILTERS_Y+3*ALIGN_FILTERS_Y_OFFSET); 
		StateComponent tunnableFilter2 = GUIWidgetBuilder.buildTunnableFilter(ComponentsOsirisMimicId.TUNNABLEFILTER2, SIZE_FILTER1_X,200,ALIGN_SLIT_X,ALIGN_FILTERS_Y+3*ALIGN_FILTERS_Y_OFFSET);
		
		
		
		components_.put(ComponentsIDs.ICMSPECTRAL, icm);
		components_.put(ComponentsIDs.ICMSPECTRALMIRROR, icmMirror);
		components_.put(ComponentsIDs.SLITSCOMPONENT, slit);
		components_.put(ComponentsIDs.COLLIMATORCOMPONENT, collimator);
		components_.put(ComponentsIDs.DATAACQUISITIONCOMPONENT, camera);
		



		components_.put(ComponentsIDs.TUNABLEFILTERSCOMPONENT1, tunnableFilter1);
		components_.put(ComponentsIDs.TUNABLEFILTERSCOMPONENT2, tunnableFilter2);
		
		this.rayLightSketchStar = GUIWidgetBuilder.buildPathStar(components_,starSketch);
		this.rayLightSketchICM = GUIWidgetBuilder.buildPathICM(components_);

		
		add(cCDTelemetryPanel);
		add(cCDWindowsPanel);
		add(statusCapsule);
		add(TemperatureAndPressurePanel);
		//add(ccdPressure);
		add(ccdStatus);
		add(ring);
		add(icm);
		add(icmMirror);
		add(slit);
		add(collimator);
		add(camera);
		add(tunnableFilter1);
		add(tunnableFilter2);

		this.rayLightSketchStarCurrent_=rayLightSketchICM;
		this.starSketch.setVisible(false);
	}

}



