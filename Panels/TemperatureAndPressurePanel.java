package gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels;

import gtc.AL.Inspector.Panels.OSIRIS.Mimic.ComponentsOsirisMimicId;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.GUIConstants;
import gtc.DSL.DAF.Services.DataConsumer;
import gtc.DSL.DAF.Services.DataViewer;
import gtc.DSL.GUI_Kit.GUIBasics.BasicsGUIDefaults;
import gtc.DSL.GUI_Kit.GUIBasics.Capsules.LabelCapsule;
import gtc.DSL.Types.IntPair;

import java.text.DecimalFormat;

import java.util.Timer;
import java.util.TimerTask;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import DGT.DoubleArray;
import org.apache.log4j.Logger;





public class TemperatureAndPressurePanel extends JPanel {
	
	final static Logger log = Logger.getLogger(TemperatureAndPressurePanel.class);
	
	private enum MONITORSCALAR {temperatureSensorA,
									temperatureSensorB,
									pressure};
		
	private DataConsumer temperatureSensorA_;
	private DataConsumer temperatureSensorB_;
	private DataConsumer pressure_;
	private LabelCapsule temperatureACapsule = new LabelCapsule("");
	private LabelCapsule temperatureBCapsule = new LabelCapsule("");
	private LabelCapsule pressureCapsule = new LabelCapsule("");
	
	private DecimalFormat myFormatter = new DecimalFormat("0.##E0");
	private DecimalFormat myFormatter1 = new DecimalFormat("###.##");
	// TODO 5239 - Cass: Revisar estos parámetros
	private short TIME_OUT_TEMP_CONNECT = 30;
	private short PERIODIC_TASK = 5000;
	private short LIMIT_WARNING_A = 300;
	private short LIMIT_ERROR_A_UP = 330;
	private short LIMIT_ERROR_A_DOWN = 130;
	private short LIMIT_WARNING_B = 84;
	private short LIMIT_ERROR_B_UP = 86;
	private short LIMIT_ERROR_B_DOWN = 72;
	private double LIMIT_ERROR_PRESSURE_DOWN = 0.00009;
	private double LIMIT_ERROR_PRESSURE_UP = 0.0009; 
	
	public TemperatureAndPressurePanel() {
		

		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		temperatureACapsule.setFont(GUIConstants.FONT_NORMAL);
		temperatureBCapsule.setFont(GUIConstants.FONT_NORMAL);
		pressureCapsule.setFont(GUIConstants.FONT_NORMAL);
		
		setBackground(BasicsGUIDefaults.BACKGROUND);
		setOpaque(true);

		add(temperatureACapsule);
		add(temperatureBCapsule);
		add(pressureCapsule);
		
		this.temperatureSensorA_ = new DataConsumer(ComponentsOsirisMimicId.LakeShoreController,"temperatureSensorA");
		MonitorsScalarConsumer tempSensorA = new MonitorsScalarConsumer(MONITORSCALAR.temperatureSensorA);
		this.temperatureSensorA_.setViewer(tempSensorA);
		CheckUpdate timerTask0 = new CheckUpdate(tempSensorA);
		
		this.temperatureSensorB_ = new DataConsumer(ComponentsOsirisMimicId.LakeShoreController,"temperatureSensorB");
		MonitorsScalarConsumer tempSensorB = new MonitorsScalarConsumer(MONITORSCALAR.temperatureSensorB);
		this.temperatureSensorB_.setViewer(tempSensorB);
		CheckUpdate timerTask1 = new CheckUpdate(tempSensorB);

		this.pressure_ = new DataConsumer(ComponentsOsirisMimicId.PFEIFFERVACUUMMON,"pressure");
		MonitorsScalarConsumer pressureConsumer = new MonitorsScalarConsumer(MONITORSCALAR.pressure);
		this.pressure_.setViewer(pressureConsumer);
		CheckUpdate timerTask2 = new CheckUpdate(pressureConsumer);
		
		temperatureACapsule.setLabel("T A: N/A " );	
		temperatureBCapsule.setLabel("T B: N/A" );	
		pressureCapsule.setLabel("P: N/A" );
		
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(timerTask2, 0, PERIODIC_TASK);
		Timer timer2 = new Timer();
		timer2.scheduleAtFixedRate(timerTask1, 0, PERIODIC_TASK);
		Timer timer3 = new Timer();
		timer3.scheduleAtFixedRate(timerTask0, 0, PERIODIC_TASK);
	}
	
	public void paintLabelA(double sample) {
		
		temperatureACapsule.setLabel("T A: " + myFormatter1.format(sample));
	}
	
	public void paintLabelB(double sample) {
		
		temperatureBCapsule.setLabel("T B: " + myFormatter1.format(sample));
	}
	
	public void paintLabelPressure(double sample) {
		
		pressureCapsule.setLabel("P: " + myFormatter.format(sample));
	}
	
	public void paintCorrectLabel(MONITORSCALAR monitor) {
		switch (monitor) {
			case temperatureSensorA:
				temperatureACapsule.setBackground(BasicsGUIDefaults.BACKGROUND);
				break;
			case temperatureSensorB:
				temperatureBCapsule.setBackground(BasicsGUIDefaults.BACKGROUND);
				break;
			case pressure:
				pressureCapsule.setBackground(BasicsGUIDefaults.BACKGROUND);
				break;
		}
	}
	
	public void paintWraningLabel(MONITORSCALAR monitor) {
		switch (monitor) {
			case temperatureSensorA:
				temperatureACapsule.setBackground(BasicsGUIDefaults.WARNING);
				break;
			case temperatureSensorB:
				temperatureBCapsule.setBackground(BasicsGUIDefaults.WARNING);
				break;
			case pressure:
				pressureCapsule.setBackground(BasicsGUIDefaults.WARNING);
				break;
		}
	}
	
	public void paintErrorLabel(MONITORSCALAR monitor) {
		switch (monitor) {
			case temperatureSensorA:
				temperatureACapsule.setBackground(BasicsGUIDefaults.ERROR);
				break;
			case temperatureSensorB:
				temperatureBCapsule.setBackground(BasicsGUIDefaults.ERROR);
				break;
			case pressure:
				pressureCapsule.setBackground(BasicsGUIDefaults.ERROR);
				break;
		}
	}
	
	public boolean checkUpdate(long lastUpdate, MONITORSCALAR monitor, double value) {
		if (lastUpdate != -1) {

			long seconds = (System.currentTimeMillis() - lastUpdate) / 1000;
			if (seconds > TIME_OUT_TEMP_CONNECT) {
				log.error("It has not been possible get data from "+ monitor + ", lastUpdate: "+ lastUpdate + ".  current: "+ System.currentTimeMillis() + ", value: " + value);
				return false;
			}
			
		}
		return true;
	}
	
	public boolean checkThrelhold(MONITORSCALAR monitor, double value) {
		
		switch (monitor) {
			case temperatureSensorA:
				paintLabelA(value);
				if ( ( value < LIMIT_ERROR_A_DOWN ) || ( value > LIMIT_ERROR_A_UP ) ) {
					log.error("The sensor A has exceeded the threholds. The opertion has been stopped, value: " + value);
					paintErrorLabel(monitor);
				}
				else if ( ( value > LIMIT_WARNING_A ) ) {
					log.warn("The sensor A has exceeded the threholds. Check the value for the operation, value: " + value);
					paintWraningLabel(monitor);
				}
				else
					paintCorrectLabel(monitor);
				break;
			case temperatureSensorB:
				paintLabelB(value);
				if ( ( value < LIMIT_ERROR_B_DOWN ) || ( value > LIMIT_ERROR_B_UP) ) {
					log.error("The sensor B has exceeded the threholds. The opertion has been stopped, value: " + value);
					paintErrorLabel(monitor);
				}
				else if ( ( value > LIMIT_WARNING_B ) ) {
					log.warn("The sensor B has exceeded the threholds. Check the value for the operation, value: " + value);
					paintWraningLabel(monitor);
				}
				else
					paintCorrectLabel(monitor);
				break;

			case pressure:
				paintLabelPressure(value);
				
				if ( (value > LIMIT_ERROR_PRESSURE_UP) && (value < LIMIT_ERROR_PRESSURE_DOWN) ) {
					log.error("The pressure sensor has exceeded the threholds. The opertion has been stopped, value: " + value);
					paintWraningLabel(monitor);
				}
				else if (value > LIMIT_ERROR_PRESSURE_DOWN) {
					log.warn("The pressure sensor has exceeded the threholds. Check the value for the operation, value: " + value);
					paintErrorLabel(monitor);
				}
				else
					paintCorrectLabel(monitor);
				break;
		}
		
		return true;
	}
	
	class MonitorsScalarConsumer implements DataViewer {
		private MONITORSCALAR monitor_;
		private double value = -100000;
		private long lastUpdate = -1;
		
		public MonitorsScalarConsumer(MONITORSCALAR monitor) {
			this.monitor_= monitor;
			this.lastUpdate = System.currentTimeMillis();
		}
		
		public long getLastUpdate () {
			return lastUpdate;
		}
		
		public MONITORSCALAR getMonitor() {
			return this.monitor_;
		}
		
		public double getValue () {
			return value;
		}
		
		public void setValue(double val) {
			value = val;
		}
		
		@Override
		public void receiveData(double sample, long timeStamp) {
			try {
				switch (monitor_) {
					case temperatureSensorA:
						lastUpdate = System.currentTimeMillis();
						checkThrelhold(monitor_,sample);
						this.value = sample;
						break;
					case temperatureSensorB:
						lastUpdate = System.currentTimeMillis();
						checkThrelhold(monitor_,sample);
						log.debug("TemperatureB: " + sample + ", "+ lastUpdate);
						this.value = sample;
						
						break;
					case pressure:
						lastUpdate = System.currentTimeMillis();
						log.debug("Pressure: " + sample + ", "+ lastUpdate);
						checkThrelhold(monitor_,sample);
						this.value = sample;
						break;
				}
			} 
			catch(Exception e) {
				e.printStackTrace();
			}
			
		}

		@Override
		public void receiveData(DoubleArray sample, long timeStamp) {
		}

		@Override
		public void receiveData(DoubleArray sample,long index,IntPair pair) {
		}	

		@Override
		public void propertyChange(int id, Object oldValue, Object newValue) {
		}
		
		
	}
	
	class CheckUpdate extends TimerTask {
		MonitorsScalarConsumer monitorConsumer;
		public int cont = 0;
		
		public CheckUpdate(MonitorsScalarConsumer monitor) {
			monitorConsumer = monitor;
			log.info("Initialize TimerTask to check both temperature and pressure sensor.");
		}
		public boolean testMimic = Boolean.valueOf(System.getenv("MIMIC_TEST2")); // TODO Cass - 5239: Revisar
		
		@Override
		public void run() {
			
			if (testMimic) 
				switch (monitorConsumer.getMonitor()) {
				case temperatureSensorA:
					if (cont % 2 == 0) {
						log.info("ChangeTemperatureA");
						monitorConsumer.setValue(159);
					}
					cont++;
					break;
				case temperatureSensorB:
					if (cont % 2 == 0) {
						log.info("ChangeTemperatureB");
						monitorConsumer.setValue(85);
					}
					cont++;
					break;

				case pressure:
					
					if (cont % 2 == 0) {
						log.info("ChangePressure");
						monitorConsumer.setValue(0.002);
					}
					cont++;
					break;
				}
			
			
			if (! checkUpdate(monitorConsumer.getLastUpdate(), monitorConsumer.getMonitor(), monitorConsumer.getValue()) )
				paintErrorLabel(monitorConsumer.getMonitor());
			else
				checkThrelhold(monitorConsumer.getMonitor(), monitorConsumer.getValue());
			
		}
		
	}
}
