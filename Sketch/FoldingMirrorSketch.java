package gtc.AL.Inspector.Panels.OSIRIS.Mimic.Sketch;

import gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.PathAbled;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.geom.Point2D;

import javax.swing.JLabel;


public class FoldingMirrorSketch extends JLabel implements ComponentListener ,PathAbled {
	
	private Point2D connectionPoint;

	public FoldingMirrorSketch() {
		this.addComponentListener(this);
	}


	public Point2D getConnectionPoint() {
		return connectionPoint;
	}
	
	public void paintComponent(Graphics2D g) {
		g.setColor(Color.GRAY);
		
		int x = (int)this.getVisibleRect().getX();
		int y = (int)this.getVisibleRect().getY();
	    int width = (int)(this.getVisibleRect().getWidth());
	    int height = (int)(this.getVisibleRect().getHeight());

		
		Polygon  polygon = new Polygon();
		
		polygon.addPoint(x, y);
		polygon.addPoint(x+width, y);
		polygon.addPoint(x, y+height);
		

		g.fill(polygon);
		

	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2d =(Graphics2D)g;
		paintComponent(g2d);
	}


	@Override
	public void componentHidden(ComponentEvent e) {
		
	}

	@Override
	public void componentMoved(ComponentEvent e) {
		selectConnectionPoint((int)e.getComponent().getX(), 
								(int)e.getComponent().getY(), 
								(int)e.getComponent().getWidth(), 
								(int)e.getComponent().getHeight());
	}

	@Override
	public void componentResized(ComponentEvent e) {
		selectConnectionPoint((int)e.getComponent().getX(), 
				(int)e.getComponent().getY(), 
				(int)e.getComponent().getWidth(), 
				(int)e.getComponent().getHeight());
	}

	@Override
	public void componentShown(ComponentEvent e) {
		selectConnectionPoint((int)e.getComponent().getX(), 
				(int)e.getComponent().getY(), 
				(int)e.getComponent().getWidth(), 
				(int)e.getComponent().getHeight());
	}
	
	private void selectConnectionPoint(int x, int y, int width,int height)
	{
		connectionPoint =  new Point2D.Double(x+width/2.0,y+height/2.0);
	}

}
