package gtc.AL.Inspector.Panels.OSIRIS.Mimic.Sketch;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;

import javax.swing.JLabel;

public class GrismSketck extends JLabel {
	
	private Color color;

	public GrismSketck(Color color) {
		this.color = color;
	}
	
	public void paintComponent(Graphics2D g) {
	    int width = (int)(this.getVisibleRect().getWidth()*0.75);
	    int height = (int)(this.getVisibleRect().getHeight()*0.5);

		int x = (int)(getVisibleRect().getWidth()/2.0-width/2.0);
		int y = (int)(this.getVisibleRect().getHeight()*0.25);
	    
		Polygon  polygon = new Polygon();
		
		g.setColor(color);
		polygon.addPoint(x, y);
		polygon.addPoint(x+width, y);
		polygon.addPoint(x+width, (int)(y+height*0.75));
		
		
		GradientPaint gradientPaint = new GradientPaint(x,y,color.darker(),x+width,y+height,color);
		g.setPaint(gradientPaint);

		
		g.fill(polygon);
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2d =(Graphics2D)g;
		paintComponent(g2d);
	}
}
