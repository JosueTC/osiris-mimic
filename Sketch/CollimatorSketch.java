package gtc.AL.Inspector.Panels.OSIRIS.Mimic.Sketch;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.RoundRectangle2D;

import javax.swing.JLabel;


public class CollimatorSketch extends JLabel {
	private Color color;
	
	
	public CollimatorSketch(Color color) {
		super();
		this.color = color;
	}

	public void paintComponent(Graphics2D g)
	{
		int x = (int)(this.getVisibleRect().getX()+this.getVisibleRect().getWidth()/3.0);
		int y = (int)(this.getVisibleRect().getY()+this.getVisibleRect().getHeight()/5.0);
	    int width = (int)(this.getVisibleRect().getWidth()/3.0);
	    int height = (int)(this.getVisibleRect().getHeight()/1.5);

		g.setColor(color);
		RoundRectangle2D.Float shape = new RoundRectangle2D.Float(x,y,width,height,10.5f,10.5f);
		Shape circle = new Ellipse2D.Float(x - 0.8f * width , y, width, height);
		Area areaShape = new Area(shape);
		Area areaCircle = new Area(circle);
		
		areaShape.subtract(areaCircle);

		GradientPaint gradientPaint = new GradientPaint(x,y,color.darker(),x+width,y+height,color);
		g.setPaint(gradientPaint);
		g.setStroke(new BasicStroke(15.0f));
		g.fill(areaShape);
		
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2d =(Graphics2D)g;
		paintComponent(g2d);
	}

}


