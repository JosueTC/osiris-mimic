package gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels;

public interface GUIPositions {
	
	public static int ALIGN_SLIT_X=500;
	public static int ALIGN_FILTERS_Y=320;
	public static int ALIGN_CAMERA_Y=710;
	public static int ALIGN_ICMMIRROR_X=100;
	public static int ALIGN_FILTERS_Y_OFFSET=60;
	public static int ALIGN_ICM_MIRROR_Y=100;
	public static int ALIGN_COLLIMATOR_X=810;
	
	public static int ALIGN_TEMPERATUR_CCD_Y=800;
	
	public static int ALIGN_CCDWINDOW_Y=650;
	
	// TODO Cass - 5239
	//public static int ALIGN_STATUS_Y=480;
	public static int ALIGN_STATUS_Y=330;
	// TODO Cass - 5239

	
	public static int SIZE_SLIT_X=180;
	public static int SIZE_FILTER1_X=500;
	public static int SIZE_FILTER1_Y=50;
	public static int SIZE_ICM_MIRROR_Y=180;
	public static int SIZE_CAMERA_Y=280;
	
	public static int ALIGN_TEMPERATUR_CCD_X=1110;
	public static int SIZE_X=750;
	public static int SIZE_X2=300;
}


