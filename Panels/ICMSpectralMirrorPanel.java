package gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels;

import gtc.AL.Inspector.Panels.OSIRIS.Mimic.ComponentsOsirisMimicId;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.GUIConstants;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.Sketch.ICMSpectralMirrorSketch;
import gtc.DSL.DAF.Services.DataConsumer;
import gtc.DSL.DAF.Services.DataViewer;
import gtc.DSL.DAF.Services.EnumConsumer;
import gtc.DSL.DAF.Services.EnumViewer;
import gtc.DSL.GUI_Kit.GUIBasics.BasicsGUIDefaults;
import gtc.DSL.GUI_Kit.GUIBasics.Capsules.LabelCapsule;
import gtc.DSL.Types.IntPair;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.geom.Point2D;

import javax.swing.BorderFactory;

import DGT.DoubleArray;
import ICMSPECTRAL.MirrorStatus_t;


/**
 * Represents the spectral mirror.
 * 
 * @author mhuertas
 *
 */
public class ICMSpectralMirrorPanel extends StateComponent implements   EnumViewer , DataViewer {

	private ICMSpectralMirrorSketch iCMSpectralMirrorSketch;
	private Point2D connectionPoint;
	private EnumConsumer mirrorStatusConsumer_;
	private MirrorStatus_t stateMirror_;
	private LabelCapsule positionName;
	private DataConsumer icmPositionConsumer_;
	private boolean errorMoving;
	private double positionOld=0;
	public ICMSpectralMirrorPanel()
	{
		super(ComponentsOsirisMimicId.ICMSPECTRAL);

		this.setOpaque(false);
		this.mirrorStatusConsumer_= new EnumConsumer(ComponentsOsirisMimicId.ICMSPECTRAL,"mirrorStatus");
		this.mirrorStatusConsumer_.setViewer(this);
		icmPositionConsumer_ = new DataConsumer(ComponentsOsirisMimicId.ICMSPECTRALMIRROR,"position");
		icmPositionConsumer_.setViewer(this);		
		this.iCMSpectralMirrorSketch = new ICMSpectralMirrorSketch(Color.WHITE);
		this.setLayout(new BorderLayout(1,1));
		this.setBackground(BasicsGUIDefaults.DARKER_GRAY);
		this.addComponentListener(this);
		this.add(iCMSpectralMirrorSketch,BorderLayout.CENTER);
		this.positionName = new LabelCapsule("Park");
		this.positionName.setFont(GUIConstants.FONT_);
		
		this.positionName.setBorder(BorderFactory.createEmptyBorder(2,2,2, 2));
		this.add(positionName,BorderLayout.NORTH);
		errorMoving=false;

	}
	

	@Override
	public Point2D getConnectionPoint() {
		return connectionPoint;
	}

	@Override
	public void propertyChange(int id, Object oldValue, Object newValue) {
	}

	@Override
	public void receiveMagnitudeChange(String component, String magnitude,
			int newState) {
			
			
		try 
		{


			stateMirror_ = ICMSPECTRAL.MirrorStatus_t.from_int(newState);
			switch(stateMirror_.value())
			{
				case ICMSPECTRAL.MirrorStatus_t._PARK:
					positionName.setLabel("Park");
					errorMoving=false;
					break;
				case ICMSPECTRAL.MirrorStatus_t._WORK:
					positionName.setLabel("Work");
					errorMoving=false;
					break;
				case ICMSPECTRAL.MirrorStatus_t._UNDEFINED:
					positionName.setLabel("Undefined");
					break;
			}
			selectBorder();
		} 
		catch(Exception e)
		{
			e.printStackTrace();
		}


	}
	
	public Status getStatus() {

		if(errorMoving) {
			return Status.ERROR;
		}
		if (stateMirror_ == ICMSPECTRAL.MirrorStatus_t.UNDEFINED) return Status.MOVING; 
		return Status.READY;
	}

	
	public void updatePosition(double x, double y, double width,double height)
	{
		connectionPoint=new Point2D.Double(x+width/2.0,y+height*0.5);
		
	    this.positionName.setMaximumSize(new Dimension((int)(width),(int)(height*0.2)));
	    this.positionName.setPreferredSize(new Dimension((int)(width),(int)(height*0.2)));
	}
	
	
	@Override
	public void receiveData(double sample, long timeStamp) {
		if (stateMirror_.value()==ICMSPECTRAL.MirrorStatus_t._UNDEFINED) {
			if (Math.abs(sample-positionOld) == 0) {
				errorMoving=true;
			} else {

				errorMoving=false;
			}
		}
		
		positionOld = sample;
		selectBorder();
	}
	
	@Override
	public void receiveData(DoubleArray sample, long timeStamp) {
	}

	@Override
	public void receiveData(DoubleArray sample,long index,IntPair pair) {
	}	


}
