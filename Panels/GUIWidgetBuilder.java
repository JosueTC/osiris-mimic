package gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels;



import static gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.GUIPositions.ALIGN_CAMERA_Y;
import static gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.GUIPositions.ALIGN_CCDWINDOW_Y;
import static gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.GUIPositions.ALIGN_FILTERS_Y;
import static gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.GUIPositions.ALIGN_FILTERS_Y_OFFSET;
import static gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.GUIPositions.ALIGN_ICMMIRROR_X;
import static gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.GUIPositions.ALIGN_ICM_MIRROR_Y;
import static gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.GUIPositions.ALIGN_SLIT_X;
import static gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.GUIPositions.ALIGN_STATUS_Y;
import static gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.GUIPositions.ALIGN_TEMPERATUR_CCD_X;
import static gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.GUIPositions.ALIGN_TEMPERATUR_CCD_Y;
import static gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.GUIPositions.SIZE_CAMERA_Y;
import static gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.GUIPositions.SIZE_FILTER1_X;
import static gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.GUIPositions.SIZE_ICM_MIRROR_Y;
import static gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.GUIPositions.SIZE_SLIT_X;
import static gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.GUIPositions.SIZE_X;
import static gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.GUIPositions.SIZE_X2;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.ComponentsOsirisMimicId;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.GUIConstants;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.Sketch.FilterSketch;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.Sketch.GrismSketck;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.Sketch.RayLightSketch;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.Sketch.StarSketch;
import gtc.DSL.CK.ComponentModel.ComponentConfigurations;
import gtc.DSL.CK.ComponentModel.Exceptions.ServiceException;
import gtc.DSL.GUI_Kit.GUIBasics.BasicsGUIDefaults;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.Map;
import java.util.Vector;

import javax.swing.JPanel;

public class GUIWidgetBuilder {
	

	
	public JPanel buildRing() {
		DomeRingPanel ring = new DomeRingPanel();
		ring.setLocation(0,0);
		ring.setSize(1100,1100);
		return ring;
	}
	
	public StateComponent buildICM() {
		ICMSpectralPanel iCMSpectral = new ICMSpectralPanel();
		iCMSpectral.setSize(new Dimension(SIZE_X2,270));
		iCMSpectral.setLocation(ALIGN_ICMMIRROR_X,ALIGN_FILTERS_Y+4*ALIGN_FILTERS_Y_OFFSET);
		
		return iCMSpectral; 
	}
	
	public StateComponent buildICMMirror() {
		ICMSpectralMirrorPanel iCMSpectralMirror = new ICMSpectralMirrorPanel();
		iCMSpectralMirror.setSize(new Dimension(SIZE_X2,SIZE_ICM_MIRROR_Y));
		iCMSpectralMirror.setLocation(ALIGN_ICMMIRROR_X,ALIGN_ICM_MIRROR_Y);
		
		return iCMSpectralMirror;
	}
	
	public StateComponent buildSlit()  {
		SlitPanel slitPanel = null;
				
		try {
			Object[] slitElementTable = (Object[])ComponentConfigurations.getPropertyValue(ComponentsOsirisMimicId.OSIRIS,"maskIdTable");
			slitPanel = new SlitPanel(slitElementTable);
			slitPanel.setSize(new Dimension(400,SIZE_ICM_MIRROR_Y));
			slitPanel.setLocation(450,ALIGN_ICM_MIRROR_Y);
		
		} catch (ServiceException e) {
			e.printStackTrace();
		}

		return slitPanel;
	}
	
	public StateComponent buildCollimator() {
		CollimatorPanel collimatorPanel = new CollimatorPanel();
		collimatorPanel.setSize(new Dimension(SIZE_SLIT_X,SIZE_ICM_MIRROR_Y));
		collimatorPanel.setLocation(870,ALIGN_ICM_MIRROR_Y);
		
		return collimatorPanel;
	}
	
	public StateComponent buildFilter(Object[] filterElementTable,String component, int size_x, int size_y,int positition_x, int position_y)  {
		OpticalWheelComponent filterPanel = null;
		
			filterPanel = new OpticalWheelComponent(component,BorderLayout.CENTER,new FilterSketch(Color.WHITE),filterElementTable);
			filterPanel.setSize(new Dimension(size_x,size_y));
			filterPanel.setLocation(positition_x,position_y);

		return filterPanel;
	}
	
	public StateComponent buildGrism(Object[] filterElementTable,String component, int size_x, int size_y,int positition_x, int position_y)  {
		OpticalWheelComponent filterPanel = null;
		
			filterPanel = new OpticalWheelComponent(component,BorderLayout.CENTER,new GrismSketck(Color.WHITE),filterElementTable);
			filterPanel.setSize(new Dimension(size_x,size_y));
			filterPanel.setLocation(positition_x,position_y);

		return filterPanel;
	}
	
	public StateComponent buildTunnableFilter(String component, int size_x, int size_y,int positition_x, int position_y)  {
		TunnableFilterPanel tunnableFilterPanel = new TunnableFilterPanel(component,BorderLayout.CENTER);
		tunnableFilterPanel.setSize(new Dimension(size_x,size_y));
		tunnableFilterPanel.setLocation(positition_x,position_y);
		tunnableFilterPanel.setVisible(false);
		
		return tunnableFilterPanel;
		
	}
	
	public StateComponent buildCamera() {
		CameraPanel cameraPanel = new CameraPanel();
		cameraPanel.setSize(new Dimension(SIZE_FILTER1_X,SIZE_CAMERA_Y));
		cameraPanel.setLocation(ALIGN_SLIT_X,ALIGN_CAMERA_Y);
		
		return cameraPanel;
	}
	
	public JPanel buildCCDTelemetry() {
		CCDTelemetryPanel cCDTelemetryPanel = new CCDTelemetryPanel();
		cCDTelemetryPanel.setSize(new Dimension(SIZE_X,350));
		cCDTelemetryPanel.setLocation(ALIGN_TEMPERATUR_CCD_X,ALIGN_TEMPERATUR_CCD_Y);
		cCDTelemetryPanel.setBackground(BasicsGUIDefaults.TRANSPARENT);
		
		return cCDTelemetryPanel;
	}
	
	public JPanel buildCCDWindowTelemetry() {
		CCDWindowsPanel cCDWindowsPanel = new CCDWindowsPanel();
		cCDWindowsPanel.setSize(new Dimension(SIZE_X,250));
		cCDWindowsPanel.setLocation(ALIGN_TEMPERATUR_CCD_X,ALIGN_CCDWINDOW_Y);
		cCDWindowsPanel.setBackground(BasicsGUIDefaults.TRANSPARENT);
		
		return cCDWindowsPanel;
	}
	
	public StarSketch buildStar() {
		StarSketch starSketch = new StarSketch(); 
		starSketch.setLocation(ALIGN_ICMMIRROR_X+100,175);
		starSketch.setSize(30,30);
		
		return starSketch;
	}

	
	public RayLightSketch buildPathStar(Map<String,StateComponent> components_,StarSketch starSketch) {
		Vector<PathAbled> pathStar = new Vector<PathAbled>(); 
		pathStar.add(starSketch);
		pathStar.add(components_.get(ComponentsIDs.SLITSCOMPONENT));
		pathStar.add(components_.get(ComponentsIDs.COLLIMATORCOMPONENT));
		//pathStar.add(foldingMirror);
		pathStar.add(components_.get(ComponentsIDs.WHEELCOMPONENT1));
		pathStar.add(components_.get(ComponentsIDs.WHEELCOMPONENT2)); 
		pathStar.add(components_.get(ComponentsIDs.WHEELCOMPONENT3));
		pathStar.add(components_.get(ComponentsIDs.WHEELCOMPONENT4));
		pathStar.add(components_.get(ComponentsIDs.DATAACQUISITIONCOMPONENT));
		
		RayLightSketch rayLightSketchStar = new RayLightSketch(pathStar,Color.YELLOW);
		
		return rayLightSketchStar; 
	}

	public RayLightSketch buildPathICM(Map<String,StateComponent> components_) {
		Vector<PathAbled> pathICM = new Vector<PathAbled>();
		pathICM.add(components_.get(ComponentsIDs.ICMSPECTRAL));
		pathICM.add(components_.get(ComponentsIDs.ICMSPECTRALMIRROR));
		pathICM.add(components_.get(ComponentsIDs.SLITSCOMPONENT));
		pathICM.add(components_.get(ComponentsIDs.COLLIMATORCOMPONENT));
		pathICM.add(components_.get(ComponentsIDs.WHEELCOMPONENT1));
		pathICM.add(components_.get(ComponentsIDs.WHEELCOMPONENT2)); 
		pathICM.add(components_.get(ComponentsIDs.WHEELCOMPONENT3));
		pathICM.add(components_.get(ComponentsIDs.WHEELCOMPONENT4));
		pathICM.add(components_.get(ComponentsIDs.DATAACQUISITIONCOMPONENT));

		RayLightSketch rayLightSketchICM= new RayLightSketch(pathICM,BasicsGUIDefaults.BLUE);
		
		return rayLightSketchICM;
	}
	
	public StatusPanel buildStatusCapsule() {
		
		StatusPanel statusPanel = new StatusPanel();
		statusPanel.setLocation(1100, ALIGN_STATUS_Y);
		// TODO Cass - 5239: Actualizar valor a 400
		statusPanel.setSize(800, 400);
		// TODO Cass - 5239
		statusPanel.setFont(GUIConstants.FONT_BIG);
		
		return statusPanel;
	}

	/*
	public ScalarMonitorCapsule buildTemperatureCapsule() {
		ScalarMonitorCapsule ccdTemperature = new ScalarMonitorCapsule("Temp",ComponentsOsirisMimicId.LakeShoreController,"temperatureSensorA");
		ccdTemperature.setSize(new Dimension(SIZE_X,120));
		ccdTemperature.setUnitsEditable(false);
		ccdTemperature.setAccuracy(2);
		ccdTemperature.setLocation(ALIGN_TEMPERATUR_CCD_X,10);
		ccdTemperature.setFont(GUIConstants.FONT_HUGE);
		
		return ccdTemperature;
	}
	
	
	
	public ScalarMonitorCapsule buildPressureCapsule() {
	
	        ScalarMonitorCapsule ccdPressure = new ScalarMonitorCapsule("Press",ComponentsOsirisMimicId.PFEIFFERVACUUMMON,"pressure");
                ccdPressure.setSize(new Dimension(SIZE_X,120));
                //ccdPressure.setUnitsEditable(false);
                //ccdPressure.setUnits(new Unit(UnitPrefix.Milli,UnitId.Bar));
                ccdPressure.setAccuracy(5);
                ccdPressure.setLocation(ALIGN_TEMPERATUR_CCD_X,140);
                ccdPressure.setFont(GUIConstants.FONT_HUGE);

                return ccdPressure;

	}*/
	
	public TemperatureAndPressurePanel buildTemperatureAndPressurePanel() {
		TemperatureAndPressurePanel temperatureAndPressurePanel = new TemperatureAndPressurePanel();
		temperatureAndPressurePanel.setSize(new Dimension(SIZE_X,250));
		temperatureAndPressurePanel.setLocation(ALIGN_TEMPERATUR_CCD_X,10);
		temperatureAndPressurePanel.setFont(GUIConstants.FONT_HUGE);
		
		return temperatureAndPressurePanel;
	}

	
	
	public CCDStatus buildCCDStatus() {
		CCDStatus ccdStatus = new CCDStatus();
		ccdStatus.setSize(new Dimension(SIZE_FILTER1_X,80));
		ccdStatus.setLocation(ALIGN_SLIT_X,ALIGN_CAMERA_Y+SIZE_CAMERA_Y+10);
		
		return ccdStatus;
	}

}

