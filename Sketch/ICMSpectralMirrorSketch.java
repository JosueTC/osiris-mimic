package gtc.AL.Inspector.Panels.OSIRIS.Mimic.Sketch;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.RoundRectangle2D;

import javax.swing.JLabel;

public class ICMSpectralMirrorSketch extends JLabel {

	private Color color;
	
	public ICMSpectralMirrorSketch(Color color) {
		super();
		this.color = color;
	}


	public void paintComponent(Graphics2D g) {

		int x =(int)(this.getVisibleRect().getX()+this.getVisibleRect().getWidth()/2.0-this.getVisibleRect().getWidth()/10.0);
		int y = 0;
	    double width = this.getVisibleRect().getWidth()/5.0;
	    double height = this.getVisibleRect().getHeight()/.9;
		
		
		AffineTransform aT = g.getTransform();
		g.setColor(color);
		
		
		Shape boundShape = new RoundRectangle2D.Double(x,y,width,height,10.5f,10.5f);
		Shape substractShape = new Ellipse2D.Double(x - 0.8f * width , y,width, height);
		Area areaBound = new Area(boundShape);
		Area areaSubstracs = new Area(substractShape);
		areaBound.subtract(areaSubstracs);

		AffineTransform at = new AffineTransform();
		at.rotate(4*Math.PI/3.0,x+width/2.0,y+height/2.0);
		at.translate(15, 8);
		g.transform(at);
		
		GradientPaint gradientPaint = new GradientPaint((float)x,
															(float)y,		
															color.darker(),
															(float)(x+width),
															(float)(y+height),
															color);
		g.setPaint(gradientPaint);
		g.fill(areaBound);

		g.setTransform(aT);
		
		
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2d =(Graphics2D)g;
		paintComponent(g2d);
	}

}
