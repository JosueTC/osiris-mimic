package gtc.AL.Inspector.Panels.OSIRIS.Mimic.CheckConfiguration;

import gtc.AL.Inspector.Panels.OSIRIS.Mimic.ComponentsOsirisMimicId;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.MarkAsBadConfiguration;
import gtc.DSL.DAF.Services.EnumConsumer;
import gtc.DSL.DAF.Services.EnumViewer;

import java.util.ArrayList;
import java.util.List;

import M3ROTATORAXIS.rotatorPos_t;

// TODO Cass - 5239: Import M3PARKING
import M3PARKING.position_t;

public class M3Conflict extends BaseConflict {
	
	private MarkAsBadConfiguration component_;
	private enum MONITORENUMS {m3}; // TODO Cass - 5239: Podría ser necesario incluir diferenciar M3 Tower y M3 Parking
	
	public M3Conflict(MarkAsBadConfiguration component,CONFLICTTYPE type) {
		super(type);
		this.component_=component;
		createMonitors();
	}
	
	private void createMonitors() {
		// TODO Cass - 5239: subscribirse al monitor de M3Parking
		//EnumConsumer m3PositionConsumer_ = new EnumConsumer(ComponentsOsirisMimicId.M3,"tourOrientation");
		//m3PositionConsumer_.setViewer(new MonitorsEnumConsumer(MONITORENUMS.m3));

		EnumConsumer m3PositionConsumer_ = new EnumConsumer(ComponentsOsirisMimicId.M3Parking,"position");
		m3PositionConsumer_.setViewer(new MonitorsEnumConsumer(MONITORENUMS.m3));
		// TODO Cass - 5239: subscribirse al monitor de M3Parking
	}
	
	class MonitorsEnumConsumer implements EnumViewer {
		private MONITORENUMS monitor_;
		public MonitorsEnumConsumer(MONITORENUMS monitor) {	
			this.monitor_=monitor;
		}
		
		@Override
		public void receiveMagnitudeChange(String component, String magnitude,int newState) {
			
			List<MarkAsBadConfiguration> inConflict = new ArrayList<MarkAsBadConfiguration>();
			List<MarkAsBadConfiguration> noConflict = new ArrayList<MarkAsBadConfiguration>();

			
			try {
				switch (monitor_) {
					case m3:
						 // TODO 5239 - Cass: Cambiar atNasmythB: el conflicto puede ser cuando M3Park está enabled!
					     /*rotatorPos_t rotatorPos = M3ROTATORAXIS.rotatorPos_t.from_int(newState);
						 if ( rotatorPos == M3ROTATORAXIS.rotatorPos_t.atNASMYTHB) {
					    	 noConflict.add(component_);
					     } else {
					    	 inConflict.add(component_);
					     }*/
						 position_t parkingPos = M3PARKING.position_t.from_int(newState);
						 if( parkingPos == position_t.PARKING_POS){
							 noConflict.add(component_);
						 }else{
							 inConflict.add(component_);
						 }
						// TODO 5239 - Cass: Cambiar atNasmythB: el conflicto puede ser cuando M3Park está enabled!
					     break;
				}
				notifyBadConfigurationListener(inConflict,noConflict,type);
			} catch (Exception e ) {
				e.printStackTrace();
			}
		}
	
		@Override
		public void propertyChange(int id, java.lang.Object oldValue,java.lang.Object newValue) {
		}
	}
}
