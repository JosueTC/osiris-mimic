package gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels;

import java.awt.geom.Point2D;

public interface PathAbled {
	public Point2D getConnectionPoint();
}
