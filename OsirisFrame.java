package gtc.AL.Inspector.Panels.OSIRIS.Mimic;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;


public class OsirisFrame extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public OsirisFrame() {
		super("Osiris Instrument Mimic");
		
		add(new OsirisMimicFolder(),BorderLayout.CENTER);
		setPreferredSize(new Dimension(1000,1000));
		setVisible(true);
		pack();
		
	}

	
	public static void main (String []args) {
		new OsirisFrame();
	}
}
