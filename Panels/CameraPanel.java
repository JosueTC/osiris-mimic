package gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels;

import gtc.AL.Inspector.Panels.OSIRIS.Mimic.ComponentsOsirisMimicId;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.Sketch.CameraSketch;
import gtc.DSL.DAF.Services.DataConsumer;
import gtc.DSL.DAF.Services.DataViewer;
import gtc.DSL.DAF.Services.EnumConsumer;
import gtc.DSL.DAF.Services.EnumViewer;
import gtc.DSL.GUI_Kit.GUIBasics.BasicsGUIDefaults;
import gtc.DSL.Types.IntPair;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.geom.Point2D;

import javax.swing.BorderFactory;

import DATAACQUISITIONCOMPONENT.OutputMode;
import DATAACQUISITIONCOMPONENT.ReadoutMode;
import DATAACQUISITIONCOMPONENT.ShutterStatus;
import DGT.DoubleArray;


/**
 * Represents the Camera
 * 
 * The readout mode, output mode and shutter status is represented. In addition, the status
 * of the camera is represented, checking the temperature and pressure limits.
 * 
 *  
 * @author mhuertas
 *
 */
public class CameraPanel extends StateComponent  {
	
	private enum MONITORBOOLEAN {currentReadoutMode,
									currentOutputMode,
									currentShutterStatus,
									isExposing,
									isReadingout};
									
	private enum MONITORSCALAR {temperatureSensorA,
									pressure};

	
	private static final double CCD_TEMPERATURE_LIMIT=178;
	private static final double CCD_PRESURE_LIMIT=0.02;

	private Point2D connectionPoint;
	private CameraSketch cameraSketch;
	
	private EnumConsumer readoutModeConsumer_;
	private EnumConsumer outputModeConsumer_;
	private EnumConsumer shutterStatusConsumer_;
	private EnumConsumer isExposingConsumer_;
	private EnumConsumer isReadingConsumer_;
	
	private DataConsumer ccdTemperatureConsumer_; 
	private DataConsumer ccdPresureConsumer_;
	private double ccdTemperature_;
	private double ccdPresure_;
	private boolean isExposing;
	private boolean isReadingout;

	
									
	public CameraPanel()
	{
		super(ComponentsOsirisMimicId.CAMERA);
		this.setOpaque(false);
		
		this.cameraSketch = new CameraSketch(Color.WHITE);
		this.cameraSketch.setBorder(BorderFactory.createEmptyBorder(10,10, 10, 10));
		this.setLayout(new BorderLayout(10,10));
		this.setBackground(BasicsGUIDefaults.DARKER_GRAY);
		this.addComponentListener(this);
		this.add(cameraSketch,BorderLayout.CENTER);
		

		
		this.readoutModeConsumer_ = new EnumConsumer(ComponentsOsirisMimicId.CAMERA, "currentReadoutMode");
		this.outputModeConsumer_ = new EnumConsumer(ComponentsOsirisMimicId.CAMERA, "currentOutputMode");
		this.shutterStatusConsumer_ = new EnumConsumer(ComponentsOsirisMimicId.CAMERA, "currentShutterStatus");
		this.isExposingConsumer_ = new EnumConsumer(ComponentsOsirisMimicId.DATAACQUISITIONCOMPONENT,"isExposing");
		this.isReadingConsumer_ = new EnumConsumer(ComponentsOsirisMimicId.DATAACQUISITIONCOMPONENT,"isReadingout");
		
		this.readoutModeConsumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.currentReadoutMode));
		this.outputModeConsumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.currentOutputMode));
		this.shutterStatusConsumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.currentShutterStatus));
		this.isExposingConsumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.isExposing));
		this.isReadingConsumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.isReadingout));
		
		
		this.ccdTemperatureConsumer_=new DataConsumer(ComponentsOsirisMimicId.LakeShoreController,"temperatureSensorA");
		this.ccdTemperatureConsumer_.setViewer(new MonitorsScalarConsumer(MONITORSCALAR.temperatureSensorA));
		this.ccdPresureConsumer_=new DataConsumer(ComponentsOsirisMimicId.PFEIFFERVACUUMMON,"pressure");
		this.ccdPresureConsumer_.setViewer(new MonitorsScalarConsumer(MONITORSCALAR.pressure));
		
	}

	public Point2D getConnectionPoint() {
		return connectionPoint;
	}

	
	protected void updatePosition(double x, double y, double width,double height)
	{
		Dimension size = getSize();
		Point2D position = getLocation();
		connectionPoint= new Point2D.Double(position.getX()+size.width*0.8,position.getY());
	}	
	

	class MonitorsBooleanConsumer implements EnumViewer
	{
		private MONITORBOOLEAN monitor_;

		private String [] isExposingLabels;
		private String [] isReadingoutLabels;
		
		public MonitorsBooleanConsumer(MONITORBOOLEAN monitor)
		{	
			this.monitor_=monitor;
		}
		@Override
		public void receiveMagnitudeChange(String component, String magnitude,
				int newState) {
			try 
			{

				switch (monitor_)
				{
					case currentReadoutMode:
						ReadoutMode readoutMode = DATAACQUISITIONCOMPONENT.ReadoutMode.from_int(newState);
						cameraSketch.setReadoutMode(readoutMode);
						repaint();
						break;
					case currentOutputMode:
						OutputMode outputMode = DATAACQUISITIONCOMPONENT.OutputMode.from_int(newState);
						cameraSketch.setOutputMode(outputMode);
						repaint();	
						break;
					case currentShutterStatus:
						ShutterStatus shutterStatus = DATAACQUISITIONCOMPONENT.ShutterStatus.from_int(newState);
						cameraSketch.setShutterStatus(shutterStatus);
						repaint();
						break;
					case isExposing:
						isExposing = new Boolean( isExposingLabels[newState] );
						break;
					case isReadingout:
						isReadingout= new Boolean( isReadingoutLabels[newState] );
						break;
				}
				selectBorder();
			} 
			catch(IndexOutOfBoundsException e)
			{
				e.printStackTrace();
			}

		}


		@Override
		public void propertyChange(int id, Object oldValue, Object newValue) {
			
			switch (monitor_)
			{
				case isExposing:
					isExposingLabels=(String[])newValue;
					break;
				case isReadingout:
					isReadingoutLabels=(String[])newValue;
					break;
			}
		}
	}
	
	
	class MonitorsScalarConsumer implements DataViewer
	{
		private MONITORSCALAR monitor_;
		
		public MonitorsScalarConsumer(MONITORSCALAR monitor)
		{	
			this.monitor_=monitor;
		}

		@Override
		public void receiveData(double sample, long timeStamp) {
			switch (monitor_)
			{
				case temperatureSensorA:
						ccdTemperature_=sample;
						repaint();
					break;
				case pressure:
						ccdPresure_=sample;
						repaint();
					break;
			}
			selectBorder();
		}
		@Override
		public void receiveData(DoubleArray sample, long timeStamp) {
		}
		@Override
		public void receiveData(DoubleArray sample,long index,IntPair pair) {
		}	

		@Override
		public void propertyChange(int id, Object oldValue, Object newValue) {
		}
	}
	/**
	 * If the temperature or pressure are wrong status error.
	 * If exposing or reading, status moving.
	 * Otherwise, status ready.
	 */
	public Status getStatus() {
		if( ccdTemperature_>CCD_TEMPERATURE_LIMIT || ccdPresure_>CCD_PRESURE_LIMIT )
			return Status.ERROR;
		if (isExposing || isReadingout)
			return Status.MOVING;
		return Status.READY;
	}
}

	


