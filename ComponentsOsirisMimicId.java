// Copyright (c) 2010 Gran Telescopio de Canarias, S.A. (GRANTECAN)
// All Rights Reserved

package gtc.AL.Inspector.Panels.OSIRIS.Mimic;

public class ComponentsOsirisMimicId
{
  public static final String ICMDOME             		= "ICM/ICMDome/ICMDome";
  public static final String ICMSPECTRAL             		= "ICM/ICMSpectralNasmythB/ICMSpectral"; // TODO 5239 - Cass: cambiar NasmythB
  public static final String ICMSPECTRALMIRROR			= "ICM/ICMSpectralNasmythB/ICMSpectralMirror"; // TODO 5239 - Cass: cambiar NasmythB
  public static final String SLITSCOMPONENT_1             	= "OSIRIS/MCS/Slits";
  public static final String TUNNABLEFILTER1            	= "OSIRIS/MCS/TFComponent_Red";
  public static final String TUNNABLEFILTER2            	= "OSIRIS/MCS/TFComponent_Blue";
  public static final String COLLIMATORCOMPONENT            	= "OSIRIS/MCS/Collimator";
  public static final String WHEELCOMPONENT_1             	= "OSIRIS/MCS/F1Wheel";
  public static final String WHEELCOMPONENT_2            	= "OSIRIS/MCS/F2Wheel";
  public static final String WHEELCOMPONENT_3             	= "OSIRIS/MCS/F3Wheel";  
  public static final String WHEELCOMPONENT_4             	= "OSIRIS/MCS/GrWheel";
  public static final String DATAACQUISITIONCOMPONENT       	= "OSIRIS/DAS/DataAcquisition_1";
  public static final String CAMERA             		= "OSIRIS/DAS/DataAcquisition_1";
  public static final String OSIRIS             		= "INSTRUMENTS/Osiris";
  public static final String LakeShoreController		= "OSIRIS/MCS/LakeShoreTempController_1";
  public static final String PFEIFFERVACUUMMON			= "OSIRIS/MCS/PfeifferVacuumMon_1";
  public static final String TFTEMPERATURA			= "OSIRIS/MCS/LakeShoreTempMon_2";
  public static final String TFHUMIDITY				= "OSIRIS/MCS/HumiditySensorComponent_2";
  public static final String FOCUS				= "OSIRIS/MCS/Focus_1";
  public static final String ROTATORB				= "MACS/NasmythRotatorB"; // TODO 5239 - Cass: cambiar por ROTATORCG
  public static final String M3	  				= "M3CS/RotatorStage";
  public static final String M2ExternalBaffle				= "M2CS/M2ExternalBaffle";
  // TODO 5239 - Cass
  public static final String M3Parking = "M3CS/ParkingStage";
  public static final String ROTATORCG = "CG/Rotator";
  // TODO 5239 - Cass
}

