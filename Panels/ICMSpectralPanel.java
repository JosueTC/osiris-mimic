package gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels;

import gtc.AL.Inspector.Panels.OSIRIS.Mimic.ComponentsOsirisMimicId;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.GUIConstants;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.Sketch.ICMLampIntensitySketch;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.Sketch.ICMLampSketch;
import gtc.DSL.DAF.Services.DataConsumer;
import gtc.DSL.DAF.Services.DataViewer;
import gtc.DSL.DAF.Services.EnumConsumer;
import gtc.DSL.DAF.Services.EnumViewer;
import gtc.DSL.GUI_Kit.GUIBasics.BasicsGUIDefaults;
import gtc.DSL.GUI_Kit.GUIBasics.Capsules.LabelCapsule;
import gtc.DSL.Types.IntPair;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.geom.Point2D;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

import DGT.DoubleArray;


/**
 * Represents the spectral lamps
 * 
 * @author mhuertas
 *
 */
public class ICMSpectralPanel extends StateComponent implements DataViewer {

	private enum MONITORBOOLEAN {isOnspectralLamp1,
		isOnspectralLamp2,
		isOnspectralLamp3,
		isOnspectralLamp4,
		isOnspectralLamp5,
		isOnspectralLamp6,
		isOnincadescentLamp};

	
	private Point2D connectionPoint;
	private Vector<ICMLampSketch> iCMSpectralLamps;
	private ICMLampIntensitySketch iCMIncandescentLamp;
	
	private LabelCapsule  labelCapsuleLamp1;
	private LabelCapsule  labelCapsuleLamp2;
	private LabelCapsule  labelCapsuleLamp3;
	private LabelCapsule  labelCapsuleLamp4;
	private LabelCapsule  labelCapsuleLamp5;
	private LabelCapsule  labelCapsuleLamp6;

	
	private EnumConsumer isOnspectralLamp1Consumer_;
	private EnumConsumer isOnspectralLamp2Consumer_;
	private EnumConsumer isOnspectralLamp3Consumer_;
	private EnumConsumer isOnspectralLamp4Consumer_;
	private EnumConsumer isOnspectralLamp5Consumer_;
	private EnumConsumer isOnspectralLamp6Consumer_;
	private EnumConsumer isOnincadescentLampConsumer_;
	
	
	private DataConsumer incadescentLampLightIntensityConsumer_;
	
	private static final int SIZELABELWIDTH =100; 
	private static final int SIZELABELHEIGHT =30;
	
	
	
	public ICMSpectralPanel() {
		super(ComponentsOsirisMimicId.ICMSPECTRAL);

		this.setOpaque(false);
		
		
		labelCapsuleLamp1 = new LabelCapsule("Xe");
		labelCapsuleLamp2 = new LabelCapsule("Ne");
		labelCapsuleLamp3 = new LabelCapsule("Hg Ar");
		labelCapsuleLamp4 = new LabelCapsule("Xe");
		labelCapsuleLamp5 = new LabelCapsule("Kr");
		labelCapsuleLamp6 = new LabelCapsule("Hg Ar");
		
		labelCapsuleLamp1.setFont(GUIConstants.FONT_);
		labelCapsuleLamp2.setFont(GUIConstants.FONT_);
		labelCapsuleLamp3.setFont(GUIConstants.FONT_);
		labelCapsuleLamp4.setFont(GUIConstants.FONT_);
		labelCapsuleLamp5.setFont(GUIConstants.FONT_);
		labelCapsuleLamp6.setFont(GUIConstants.FONT_);
		


		labelCapsuleLamp1.setSize(SIZELABELWIDTH,SIZELABELHEIGHT);
		labelCapsuleLamp2.setSize(SIZELABELWIDTH,SIZELABELHEIGHT);
		labelCapsuleLamp3.setSize(SIZELABELWIDTH,SIZELABELHEIGHT);
		
		
		ICMLampSketch iCMSpectralLamp1 = new ICMLampSketch(65,65);
		ICMLampSketch iCMSpectralLamp2 = new ICMLampSketch(65,65);
		ICMLampSketch iCMSpectralLamp3 = new ICMLampSketch(65,65);
		ICMLampSketch iCMSpectralLamp4 = new ICMLampSketch(65,65);
		ICMLampSketch iCMSpectralLamp5 = new ICMLampSketch(65,65);
		ICMLampSketch iCMSpectralLamp6 = new ICMLampSketch(65,65);
		
		iCMSpectralLamps = new Vector<ICMLampSketch>();
		
		iCMSpectralLamps.add(iCMSpectralLamp1);
		iCMSpectralLamps.add(iCMSpectralLamp2);
		iCMSpectralLamps.add(iCMSpectralLamp3);
		iCMSpectralLamps.add(iCMSpectralLamp4);
		iCMSpectralLamps.add(iCMSpectralLamp5);
		iCMSpectralLamps.add(iCMSpectralLamp6);		
		


		iCMIncandescentLamp = new ICMLampIntensitySketch(getWidth(),30);
	
		
		this.isOnspectralLamp1Consumer_ = new EnumConsumer(ComponentsOsirisMimicId.ICMSPECTRAL, "isOn_spectralLamp1");
		this.isOnspectralLamp2Consumer_ = new EnumConsumer(ComponentsOsirisMimicId.ICMSPECTRAL, "isOn_spectralLamp2");
		this.isOnspectralLamp3Consumer_ = new EnumConsumer(ComponentsOsirisMimicId.ICMSPECTRAL, "isOn_spectralLamp3");
		this.isOnspectralLamp4Consumer_ = new EnumConsumer(ComponentsOsirisMimicId.ICMSPECTRAL, "isOn_spectralLamp4");
		this.isOnspectralLamp5Consumer_ = new EnumConsumer(ComponentsOsirisMimicId.ICMSPECTRAL, "isOn_spectralLamp5");
		this.isOnspectralLamp6Consumer_ = new EnumConsumer(ComponentsOsirisMimicId.ICMSPECTRAL, "isOn_spectralLamp6");
		this.isOnincadescentLampConsumer_ = new EnumConsumer(ComponentsOsirisMimicId.ICMSPECTRAL, "isOn_incadescentLamp");
		this.incadescentLampLightIntensityConsumer_= new DataConsumer(ComponentsOsirisMimicId.ICMSPECTRAL,"incadescentLampLightIntensity");

		this.isOnspectralLamp1Consumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.isOnspectralLamp1));
		this.isOnspectralLamp2Consumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.isOnspectralLamp2));
		this.isOnspectralLamp3Consumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.isOnspectralLamp3));
		this.isOnspectralLamp4Consumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.isOnspectralLamp4));
		this.isOnspectralLamp5Consumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.isOnspectralLamp5));
		this.isOnspectralLamp6Consumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.isOnspectralLamp6));
		this.isOnincadescentLampConsumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.isOnincadescentLamp));		
		this.incadescentLampLightIntensityConsumer_.setViewer(this);
		
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		setLayout(new GridBagLayout());
		

		/**
		 * Since there are six lamps, but just two different types.
		 * The lamps have been group in the same row by type.
		 * 
		*/
		
		c.gridx = 0;
		c.gridy = 0;
		c.anchor = GridBagConstraints.CENTER; 

		add(buildPanel(labelCapsuleLamp1),c);
		
		c.gridx = 1;
		c.gridy = 0;
		add(iCMSpectralLamp1,c);
		
		c.gridx = 2;
		c.gridy = 0;
		add(buildPanel(labelCapsuleLamp4),c);
		
		c.gridx = 3;
		c.gridy = 0;
		add(iCMSpectralLamp2,c);
		
		c.gridx = 0;
		c.gridy = 1;
		add(buildPanel(labelCapsuleLamp2),c);
		
		c.gridx = 1;
		c.gridy = 1;
		add(iCMSpectralLamp3,c);
		
		c.gridx = 2;
		c.gridy = 1;
		add(buildPanel(labelCapsuleLamp5),c);
		
		c.gridx = 3;
		c.gridy = 1;
		add(iCMSpectralLamp4,c);

		c.gridx = 0;
		c.gridy = 2;
		add(buildPanel(labelCapsuleLamp3),c);

		c.gridx = 1;
		c.gridy = 2;
		add(iCMSpectralLamp5,c);
		
		c.gridx = 2;
		c.gridy = 2;
		add(buildPanel(labelCapsuleLamp6),c);

		c.gridx = 3;
		c.gridy = 2;
		add(iCMSpectralLamp6,c);
		
		
		
		c.gridx = 0;
		c.gridy = 3;
		c.gridwidth = 4;
		add(Box.createVerticalStrut(10),c);

		c.gridx = 0;
		c.gridy = 4;
		c.gridwidth = 4;
		add(iCMIncandescentLamp,c);

		this.addComponentListener(this);
	}
	
	private JPanel buildPanel(LabelCapsule  labelCapsuleLamp)
	{
		JPanel panel= new JPanel();
		panel.setLayout(new BoxLayout(panel,BoxLayout.X_AXIS));
		panel.setBackground(BasicsGUIDefaults.TRANSPARENT);
		labelCapsuleLamp.setPreferredSize(new Dimension(60,30));
		labelCapsuleLamp.setSize(new Dimension(60,30));
		labelCapsuleLamp.setMaximumSize(new Dimension(60,30));

		panel.add(labelCapsuleLamp);
		
		return panel;
		
	}
	public Point2D getConnectionPoint() {
		return connectionPoint;
	}
	
	class MonitorsBooleanConsumer implements EnumViewer
	{
		private MONITORBOOLEAN monitor_;
		
		private String [] isOnspectralLamp1Labels;
		private String [] isOnspectralLamp2Labels;
		private String [] isOnspectralLamp3Labels;
		private String [] isOnspectralLamp4Labels;
		private String [] isOnspectralLamp5Labels;
		private String [] isOnspectralLamp6Labels;
		private String [] isOnincadescentLampLabels;
		
		
		public MonitorsBooleanConsumer(MONITORBOOLEAN monitor)
		{	
			this.monitor_=monitor;
		}

		@Override
		public void receiveMagnitudeChange(String component, String magnitude,int newState) {

			try 
			{
				switch (monitor_)
				{
					case isOnspectralLamp1:
						
						Boolean isOnspectralLamp1 = new Boolean( isOnspectralLamp1Labels[newState] );
						ICMLampSketch iCMLampSketch1 = iCMSpectralLamps.get(0);
						if (iCMLampSketch1!=null)
							iCMLampSketch1.setOn(isOnspectralLamp1);
						break;

					case isOnspectralLamp2:
						Boolean isOnspectralLamp2 = new Boolean( isOnspectralLamp2Labels[newState] );
						ICMLampSketch iCMLampSketch2 = iCMSpectralLamps.get(2);
						if (iCMLampSketch2!=null)
							iCMLampSketch2.setOn(isOnspectralLamp2);
						break;
					case isOnspectralLamp3:
						Boolean isOnspectralLamp3 = new Boolean( isOnspectralLamp3Labels[newState] );
						ICMLampSketch iCMLampSketch3 = iCMSpectralLamps.get(4);
						if (iCMLampSketch3!=null)
							iCMLampSketch3.setOn(isOnspectralLamp3);
						break;
					case isOnspectralLamp4:
						Boolean isOnspectralLamp4 = new Boolean( isOnspectralLamp4Labels[newState] );
						ICMLampSketch iCMLampSketch4 = iCMSpectralLamps.get(1);
						if (iCMLampSketch4!=null)
							iCMLampSketch4.setOn(isOnspectralLamp4);
						break;
					case isOnspectralLamp5:
						Boolean isOnspectralLamp5 = new Boolean( isOnspectralLamp5Labels[newState] );
						ICMLampSketch iCMLampSketch5 = iCMSpectralLamps.get(3);
						if (iCMLampSketch5!=null)
							iCMLampSketch5.setOn(isOnspectralLamp5);
						break;
					case isOnspectralLamp6:
						Boolean isOnspectralLamp6 = new Boolean( isOnspectralLamp6Labels[newState] );
						ICMLampSketch iCMLampSketch6 = iCMSpectralLamps.get(5);
						if (iCMLampSketch6!=null)
							iCMLampSketch6.setOn(isOnspectralLamp6);
						break;
					case isOnincadescentLamp:
						Boolean isOnincadescentLamp = new Boolean( isOnincadescentLampLabels[newState] );
						iCMIncandescentLamp.setOn(isOnincadescentLamp);
						break;
				}
				selectBorder();
				repaint();

			} 
			catch(Exception e)
			{
				e.printStackTrace();
			}

		}

		@Override
		public void propertyChange(int id, Object oldValue, Object newValue) {
			switch (monitor_)
			{
				case isOnspectralLamp1:
					isOnspectralLamp1Labels = (String[])newValue;
					break;
				case isOnspectralLamp2:
					isOnspectralLamp2Labels = (String[])newValue;
					break;
				case isOnspectralLamp3:
					isOnspectralLamp3Labels = (String[])newValue;
					break;
				case isOnspectralLamp4:
					isOnspectralLamp4Labels = (String[])newValue;
					break;
				case isOnspectralLamp5:
					isOnspectralLamp5Labels = (String[])newValue;
					break;
				case isOnspectralLamp6:
					isOnspectralLamp6Labels = (String[])newValue;
					break;
				case isOnincadescentLamp:
					isOnincadescentLampLabels = (String[])newValue;
					break;
			}
		}
	}

	@Override
	public void receiveData(double sample, long timeStamp) {
		iCMIncandescentLamp.setIntensityLight(sample);
		repaint();
	}

	@Override
	public void receiveData(DoubleArray sample, long timeStamp) {
		
	}

	@Override
	public void receiveData(DoubleArray sample,long index,IntPair pair) {
	}	


	@Override
	public void propertyChange(int id, Object oldValue, Object newValue) {
		
	}

	@Override
	public Status getStatus() {
		return Status.READY;
	}

	
	

	public void updatePosition(double x, double y, double width,double height)
	{
		this.connectionPoint= new Point2D.Double(x+width*0.5,y);
	}
}



