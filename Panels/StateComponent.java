package gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels;

import gtc.DSL.CK.ComponentModel.ComponentState;
import gtc.DSL.DAF.Services.StateConsumer;
import gtc.DSL.DAF.Services.StateViewer;
import gtc.DSL.GUI_Kit.GUIBasics.BasicsGUIDefaults;

import java.awt.Color;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.JPanel;

import DGT.State_t;

public abstract class StateComponent extends JPanel  implements StateViewer,PathAbled,ComponentListener,MarkAsBadConfiguration {
	public enum Status {READY,ERROR,MOVING,BADCONFIGURATION};
	
	public static final Color TRANSLUCID_RED = new Color(205, 56 , 36,170);
	public static final Color TRANSLUCID_YELLOW = new Color(255, 255 ,36,200);
	public static final Color TRANSLUCID_BLUE = new Color(102, 153, 255,170);
	public static final Color TRANSLUCID_ORANGE = new Color(255, 204, 0,200);
	
	private StateConsumer stateConsumer_;
	private ComponentState state_ = ComponentState.IDLE;
	private boolean badConfiguration_;
	private String component; 
	
	public StateComponent(String component)
	{
		this.component=component;
		this.stateConsumer_ = new StateConsumer(component);
		this.stateConsumer_.setViewer(this);
		
	}

	@Override
	public void receiveStateChange(String component, State_t newState) {
		
		String state = newState.scoped_name;
		this.state_=ComponentState.valueOf(state);
		this.selectBorder();
	}
	public void propertyChange(int id, Object oldValue, Object newValue) {
	}


	public void selectBorder()
	{
		Status status_=Status.READY;
		
		switch(this.state_)
		{
			case STARTING:
			case INITIALISING:
			case RUN:
			case HALTING:
			case RESETTING:
				status_= Status.MOVING;	
				break;

			case FAULT:
			case OFF:
			case INACTIVE:
			case DISABLED:
			case ON:
				status_= Status.ERROR;
				break;
		}

		
		if (getStatus()==Status.ERROR || status_==Status.ERROR )
		{
			this.setBorder(new RoundedBorder(StateComponent.TRANSLUCID_RED));
			return;
		}
		if (getStatus()==Status.MOVING || status_==Status.MOVING )
		{
			this.setBorder(new RoundedBorder(StateComponent.TRANSLUCID_YELLOW));
			return;
		}
		if (badConfiguration_ )
		{
			this.setBorder(new RoundedBorder(StateComponent.TRANSLUCID_ORANGE));
			return;
		}
		this.setBorder(new RoundedBorder(BasicsGUIDefaults.DARKER_GRAY));
		
	}

	public void setBadConfiguration(boolean badConfiguration) {
		badConfiguration_ = badConfiguration;
		selectBorder();
	}

	@Override
	public void componentHidden(ComponentEvent arg0) {
	}

	@Override
	public void componentMoved(ComponentEvent e) {
		double x = e.getComponent().getX();
		double y = e.getComponent().getY();
	    double width = e.getComponent().getWidth();
	    double height = e.getComponent().getHeight();
	    
	    updatePosition(x,y,width,height);
		
	}

	@Override
	public void componentResized(ComponentEvent e) {
		double x = e.getComponent().getX();
		double y = e.getComponent().getY();
	    double width = e.getComponent().getWidth();
	    double height = e.getComponent().getHeight();
	    
	    updatePosition(x,y,width,height);
		
	}

	@Override
	public void componentShown(ComponentEvent e) {
		double x = e.getComponent().getX();
		double y = e.getComponent().getY();
	    double width = e.getComponent().getWidth();
	    double height = e.getComponent().getHeight();
	    
	    updatePosition(x,y,width,height);
		
	}
	
	public abstract Status getStatus();
	protected abstract void updatePosition(double x, double y, double width,double height);
}