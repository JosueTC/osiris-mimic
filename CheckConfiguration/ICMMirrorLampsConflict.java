package gtc.AL.Inspector.Panels.OSIRIS.Mimic.CheckConfiguration;

import gtc.AL.Inspector.Panels.OSIRIS.Mimic.ComponentsOsirisMimicId;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.ComponentsIDs;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.MarkAsBadConfiguration;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.StateComponent;
import gtc.DSL.DAF.Services.EnumConsumer;
import gtc.DSL.DAF.Services.EnumViewer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.ObjectUtils.Null;
import org.apache.log4j.Logger;

import ICMSPECTRAL.MirrorStatus_t;



/**
 * It detects if the current configuration is forgiven, marking the elements
 * in conflict. 
 * 
 * @author mhuertas
 *
 */
public class ICMMirrorLampsConflict extends BaseConflict {
	
	final static Logger log = Logger.getLogger(ICMMirrorLampsConflict.class);
	
	private enum MONITORBOOLEAN {
		icmMirrorStatus,
		isOnspectralLamp1,
		isOnspectralLamp2,
		isOnspectralLamp3,
		isOnspectralLamp4,
		isOnspectralLamp5,
		isOnspectralLamp6,
		isOnincadescentLamp		
	};
	
	private Map<String,StateComponent> componentsToBeNotified_;
	
	private EnumConsumer icmMirrorStatusConsumer_;
	private EnumConsumer isOnspectralLamp1Consumer_;
	private EnumConsumer isOnspectralLamp2Consumer_;
	private EnumConsumer isOnspectralLamp3Consumer_;
	private EnumConsumer isOnspectralLamp4Consumer_;
	private EnumConsumer isOnspectralLamp5Consumer_;
	private EnumConsumer isOnspectralLamp6Consumer_;
	private EnumConsumer isOnincadescentLampConsumer_;

	private Boolean isOnspectralLamp1 = false;
	private Boolean isOnspectralLamp2 = false;
	private Boolean isOnspectralLamp3 = false;
	private Boolean isOnspectralLamp4 = false;
	private Boolean isOnspectralLamp5 = false;
	private Boolean isOnspectralLamp6 = false;
	private Boolean isOnincadescentLamp = false;
	private MirrorStatus_t stateMirror_ = MirrorStatus_t.UNDEFINED;
	
	public ICMMirrorLampsConflict(Map<String,StateComponent> components_,CONFLICTTYPE type) {
		super(type);
		
		this.componentsToBeNotified_= components_;
		createMonitors();
	}
	
	private void createMonitors() {
		
		icmMirrorStatusConsumer_ = new EnumConsumer(ComponentsOsirisMimicId.ICMSPECTRAL,"mirrorStatus");
		isOnspectralLamp1Consumer_ = new EnumConsumer(ComponentsOsirisMimicId.ICMSPECTRAL, "isOn_spectralLamp1");
		isOnspectralLamp2Consumer_ = new EnumConsumer(ComponentsOsirisMimicId.ICMSPECTRAL, "isOn_spectralLamp2");
		isOnspectralLamp3Consumer_ = new EnumConsumer(ComponentsOsirisMimicId.ICMSPECTRAL, "isOn_spectralLamp3");
		isOnspectralLamp4Consumer_ = new EnumConsumer(ComponentsOsirisMimicId.ICMSPECTRAL, "isOn_spectralLamp4");
		isOnspectralLamp5Consumer_ = new EnumConsumer(ComponentsOsirisMimicId.ICMSPECTRAL, "isOn_spectralLamp5");
		isOnspectralLamp6Consumer_ = new EnumConsumer(ComponentsOsirisMimicId.ICMSPECTRAL, "isOn_spectralLamp6");
		isOnincadescentLampConsumer_ = new EnumConsumer(ComponentsOsirisMimicId.ICMSPECTRAL, "isOn_incadescentLamp");

		icmMirrorStatusConsumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.icmMirrorStatus));
		isOnspectralLamp1Consumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.isOnspectralLamp1));
		isOnspectralLamp2Consumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.isOnspectralLamp2));
		isOnspectralLamp3Consumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.isOnspectralLamp3));
		isOnspectralLamp4Consumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.isOnspectralLamp4));
		isOnspectralLamp5Consumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.isOnspectralLamp5));
		isOnspectralLamp6Consumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.isOnspectralLamp6));
		isOnincadescentLampConsumer_.setViewer(new MonitorsBooleanConsumer(MONITORBOOLEAN.isOnincadescentLamp));
	}
	
	class MonitorsBooleanConsumer implements EnumViewer {
		private MONITORBOOLEAN monitor_;

		private String [] icmMirrorStatusLabels;
		private String [] isOnspectralLamp1Labels;
		private String [] isOnspectralLamp2Labels;
		private String [] isOnspectralLamp3Labels;
		private String [] isOnspectralLamp4Labels;
		private String [] isOnspectralLamp5Labels;
		private String [] isOnspectralLamp6Labels;
		private String [] isOnincadescentLampLabels;

		
		public MonitorsBooleanConsumer(MONITORBOOLEAN monitor) {	
			this.monitor_=monitor;
		}

		@Override
		public void receiveMagnitudeChange(String component, String magnitude,int newState) {
			
			List<MarkAsBadConfiguration> inConflict = new ArrayList<MarkAsBadConfiguration>();
			List<MarkAsBadConfiguration> noConflict = new ArrayList<MarkAsBadConfiguration>();

			try{
				switch(monitor_)
				{
					case icmMirrorStatus:
						stateMirror_ = ICMSPECTRAL.MirrorStatus_t.from_int(newState);
						if (stateMirror_ == null) {
							log.error("stateMirror is null, it has received a bad state");
							return;
						}
							
							
							
						break;
					case isOnspectralLamp1:
						if (newState < 0 || newState >= isOnspectralLamp1Labels.length) {
							log.error("Index out of bounds when was trying to access to isOnspectralLamp1Labels");
							return;
						}
							
						isOnspectralLamp1 = new Boolean( isOnspectralLamp1Labels[newState] );
						break;
					case isOnspectralLamp2:
						if (newState < 0 || newState >= isOnspectralLamp2Labels.length) {
							log.error("Index out of bounds when was trying to access to isOnspectralLamp2Labels");
							return;
						}
						isOnspectralLamp2 = new Boolean( isOnspectralLamp2Labels[newState] );
						break;
					case isOnspectralLamp3:
						if (newState < 0 || newState >= isOnspectralLamp3Labels.length) {
							log.error("Index out of bounds when was trying to access to isOnspectralLamp3Labels");
							return;
						}
						isOnspectralLamp3 = new Boolean( isOnspectralLamp3Labels[newState] );
						break;
					case isOnspectralLamp4:
						if (newState < 0 || newState >= isOnspectralLamp4Labels.length) {
							log.error("Index out of bounds when was trying to access to isOnspectralLamp4Labels");
							return;
						}
						isOnspectralLamp4 = new Boolean( isOnspectralLamp4Labels[newState] );
						break;
					case isOnspectralLamp5:
						if (newState < 0 || newState >= isOnspectralLamp5Labels.length) {
							log.error("Index out of bounds when was trying to access to isOnspectralLamp5Labels");
							return;
						}
						isOnspectralLamp5 = new Boolean( isOnspectralLamp5Labels[newState] );
						break;
					case isOnspectralLamp6:
						if (newState < 0 || newState >= isOnspectralLamp6Labels.length) {
							log.error("Index out of bounds when was trying to access to isOnspectralLamp6Labels");
							return;
						}
						isOnspectralLamp6 = new Boolean( isOnspectralLamp6Labels[newState] );
						break;
					case isOnincadescentLamp:
						if (newState < 0 || newState >= isOnincadescentLampLabels.length) {
							log.error("Index out of bounds when was trying to access to isOnincadescentLampLabels");
							return;
						}
						isOnincadescentLamp = new Boolean( isOnincadescentLampLabels[newState] );
						break;
				}
				//ICM lamps encendida (alguna) + ICM mirror en PARK => Las lamparas del ICM
				if (stateMirror_ == ICMSPECTRAL.MirrorStatus_t.PARK && 
						(isOnspectralLamp1 || 
								isOnspectralLamp2 || 
								isOnspectralLamp3 || 
								isOnspectralLamp4 || 
								isOnspectralLamp5 ||
								isOnspectralLamp6 ||
								isOnincadescentLamp) ) {
				
					StateComponent stateComponet = componentsToBeNotified_.get(ComponentsIDs.ICMSPECTRAL);
					inConflict.add(stateComponet);
					 
				}
				else  {
					StateComponent stateComponet = componentsToBeNotified_.get(ComponentsIDs.ICMSPECTRAL);
					noConflict.add(stateComponet);
				}
				notifyBadConfigurationListener(inConflict,noConflict,type);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		public void propertyChange(int id, java.lang.Object oldValue,java.lang.Object newValue) {
				
			switch (monitor_)
			{
				case icmMirrorStatus:
					icmMirrorStatusLabels= (String[])newValue;
					break;
				case isOnspectralLamp1:
					isOnspectralLamp1Labels = (String[])newValue;
					break;
				case isOnspectralLamp2:
					isOnspectralLamp2Labels = (String[])newValue;
					break;
				case isOnspectralLamp3:
					isOnspectralLamp3Labels = (String[])newValue;
					break;
				case isOnspectralLamp4:
					isOnspectralLamp4Labels = (String[])newValue;
					break;
				case isOnspectralLamp5:
					isOnspectralLamp5Labels = (String[])newValue;
					break;
				case isOnspectralLamp6:
					isOnspectralLamp6Labels = (String[])newValue;
					break;
				case isOnincadescentLamp:
					isOnincadescentLampLabels = (String[])newValue;
					break;
			}
		}
	}
	
}
