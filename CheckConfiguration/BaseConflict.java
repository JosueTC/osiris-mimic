package gtc.AL.Inspector.Panels.OSIRIS.Mimic.CheckConfiguration;

import gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.BadConfigurationListener;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.MarkAsBadConfiguration;

import java.util.ArrayList;
import java.util.List;

public class BaseConflict {
	
	private List<BadConfigurationListener> listeners_ = new ArrayList<BadConfigurationListener>();
	
	public enum CONFLICTTYPE {
		M3, // TODO 5239 - M3 Torre
		M2,
		ICM,
		OPTIC,
		M3PARK
	}
	
	protected CONFLICTTYPE type;
	
	public BaseConflict(CONFLICTTYPE type) {
		this.type=type;
	}
	
	public void addBadConfigurationListener(BadConfigurationListener listener) {
		listeners_.add(listener);
	}
	
	public void notifyBadConfigurationListener(List<MarkAsBadConfiguration> inConflict,
													List<MarkAsBadConfiguration> noConflict ,
													CONFLICTTYPE type) {
		for (BadConfigurationListener listener : listeners_ ) {
			listener.notifyBadConfiguration(inConflict, noConflict,type);
		}
	}

}
