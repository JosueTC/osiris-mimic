package gtc.AL.Inspector.Panels.OSIRIS.Mimic.Sketch;



import gtc.DSL.GUI_Kit.GUIBasics.BasicsGUIDefaults;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;

import javax.swing.JLabel;

public class ICMLampSketch extends JLabel {

	protected boolean isOn;
	private final double percentFirstCircle=0.9;
	private final double percentSecondCircle=0.7;


	public ICMLampSketch(int width, int height) {
		this.setSize(width,height);
		this.setPreferredSize(new Dimension(width,height));
	}

	public void setOn(boolean isOn) {
		this.isOn = isOn;
	}

	public void draw(Graphics2D g)
	{
		
		int x = (int)this.getX();
		int y = (int)this.getY();
		int width = (int)(this.getWidth());
		int height = (int)(this.getHeight());

		draw( g,  x,  y,  width, height);
		
	}
	
	public void draw(Graphics2D g, int x, int y, int width, int height)
	{
		g.setColor(BasicsGUIDefaults.DARKER_GRAY);
		Shape ExternalCircle = new  Ellipse2D.Double(x,y,width,height);
		g.fill(ExternalCircle);
		
		Shape InternalCircle = new  Ellipse2D.Double(x+width*(1-percentFirstCircle)/2.0,
				y+height*(1-percentFirstCircle)/2.0,
				width*(percentFirstCircle),
				height*(percentFirstCircle));
		g.setColor(Color.BLACK);
		g.fill(InternalCircle);

		
		Shape IluminatedCircle = new  Ellipse2D.Double(x+width*(1-percentSecondCircle)/2.0,
				y+height*(1-percentSecondCircle)/2.0,
				width*(percentSecondCircle),
				height*(percentSecondCircle));
		if (isOn ) g.setColor(Color.YELLOW); else g.setColor(Color.GRAY); 
		g.fill(IluminatedCircle);	
		
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d =(Graphics2D)g;
		
		int x = (int)this.getVisibleRect().getX();
		int y = (int)this.getVisibleRect().getY();
		int width = (int)(this.getWidth());
		int height = (int)(this.getHeight());

		draw( g2d,  x,  y,  width, height);

	}
}
