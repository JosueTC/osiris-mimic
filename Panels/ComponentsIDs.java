package gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels;

public class ComponentsIDs {

	public static final String ICMDOME = "ICMDOME";
	public static final String ICMSPECTRAL = "ICMSPECTRAL"; // TODO Cass - 5239: Revisar el uso de ICMSPECTRAL
	public static final String ICMSPECTRALMIRROR = "ICMSPECTRALMIRROR";
	public static final String SLITSCOMPONENT = "SLITSCOMPONENT";
	public static final String COLLIMATORCOMPONENT = "COLLIMATORCOMPONENT";
	public static final String WHEELCOMPONENT1 = "WHEELCOMPONENT1";
	public static final String WHEELCOMPONENT2 = "WHEELCOMPONENT2";
	public static final String WHEELCOMPONENT3 = "WHEELCOMPONENT3";
	public static final String WHEELCOMPONENT4 = "WHEELCOMPONENT4";
	public static final String TUNABLEFILTERSCOMPONENT1 = "TUNABLEFILTERSCOMPONENT1";
	public static final String TUNABLEFILTERSCOMPONENT2 = "TUNABLEFILTERSCOMPONENT2";
	public static final String DATAACQUISITIONCOMPONENT = "DATAACQUISITIONCOMPONENT";
	public static final String M3TOWER = "M3TOWER";

	// TODO Cass - 5239: ¿Incluir M3 Parking?

}
