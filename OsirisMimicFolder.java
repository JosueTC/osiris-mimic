package gtc.AL.Inspector.Panels.OSIRIS.Mimic;

import gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels.OsirisMimicPanel;
import gtc.DSL.GUI_Kit.Folders.Folder;

import java.awt.BorderLayout;


public class OsirisMimicFolder extends Folder  {
	public OsirisMimicFolder()
	{
		OsirisMimicPanel osirisMimicSketch = new OsirisMimicPanel();

		this.setLayout(new BorderLayout());	
    		this.add(osirisMimicSketch,BorderLayout.CENTER);
	}	
	
		

}




