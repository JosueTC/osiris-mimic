package gtc.AL.Inspector.Panels.OSIRIS.Mimic.Panels;

import gtc.AL.Inspector.Panels.OSIRIS.Mimic.GUIConstants;
import gtc.AL.Inspector.Panels.OSIRIS.Mimic.CheckConfiguration.BaseConflict.CONFLICTTYPE;
import gtc.DSL.GUI_Kit.GUIBasics.BasicsGUIDefaults;
import gtc.DSL.GUI_Kit.GUIBasics.Capsules.LabelCapsule;

import java.awt.*;
import java.util.List;

import javax.persistence.Basic;
import javax.swing.JPanel;

public class StatusPanel extends JPanel implements BadConfigurationListener {
	private static int SIZE=180;
	//private static int SIZE=155;
	
	private LabelCapsule m3StatusCapsule;
	private LabelCapsule m2StatusCapsule;
	private LabelCapsule icmStatusCapsule;
	private LabelCapsule opticalStatusCapsule;
	// TODO Cass - 5239
	private LabelCapsule m3ParkStatusCapsule;
	
	public StatusPanel() {
		this.setOpaque(false);
		setBackground(BasicsGUIDefaults.BACKGROUND);
	
		
		buildGUI();
	}

	private void buildGUI() {
		// TODO Cass - 5239
		m3ParkStatusCapsule = new LabelCapsule("M3 Park");
		m3ParkStatusCapsule.setPreferredSize(new Dimension(SIZE, SIZE));
		m3ParkStatusCapsule.setFont(GUIConstants.FONT_MEDIUM);
		add(m3ParkStatusCapsule);
		// TODO Cass - 5239
		
		m2StatusCapsule = new LabelCapsule("M2 Baffles");
		m2StatusCapsule.setPreferredSize(new Dimension(SIZE, SIZE));
		m2StatusCapsule.setFont(GUIConstants.FONT_MEDIUM);
		add(m2StatusCapsule);
		
		icmStatusCapsule = new LabelCapsule("ICM Lamps");
		icmStatusCapsule.setPreferredSize(new Dimension(SIZE, SIZE));
		icmStatusCapsule.setFont(GUIConstants.FONT_MEDIUM);
		add(icmStatusCapsule);

		opticalStatusCapsule = new LabelCapsule("Optic");
		opticalStatusCapsule.setPreferredSize(new Dimension(SIZE, SIZE));
		opticalStatusCapsule.setFont(GUIConstants.FONT_MEDIUM);
		add(opticalStatusCapsule);

		// TODO Cass - 5239
		m3StatusCapsule = new LabelCapsule("M3 Tower");
		m3StatusCapsule.setPreferredSize(new Dimension(SIZE, SIZE));
		m3StatusCapsule.setFont(GUIConstants.FONT_MEDIUM);
		add(m3StatusCapsule);
		// TODO Cass - 5239
	}

	@Override
	public void notifyBadConfiguration(List<MarkAsBadConfiguration> inConflict,List<MarkAsBadConfiguration> noConflict,CONFLICTTYPE type) {
		if (inConflict.size()!=0) {
			switch(type) {
				case M3:
					m3StatusCapsule.setBackground(StateComponent.TRANSLUCID_RED);
					break;
				case M2:
					m2StatusCapsule.setBackground(StateComponent.TRANSLUCID_ORANGE);
					break;
				case ICM:
					icmStatusCapsule.setBackground(StateComponent.TRANSLUCID_RED);
					break;
				case OPTIC:
					opticalStatusCapsule.setBackground(StateComponent.TRANSLUCID_ORANGE);
					break;
				// TODO Cass - 5239
				case M3PARK:
					m3ParkStatusCapsule.setBackground(StateComponent.TRANSLUCID_RED); // ¿Color?
					break;
				// TODO Cass - 5239
			}
		} else {
			switch(type) {
				case M3:
					m3StatusCapsule.setBackground(BasicsGUIDefaults.BACKGROUND);
					break;
				case M2:
					m2StatusCapsule.setBackground(BasicsGUIDefaults.BACKGROUND);
					break;
				case ICM:
					icmStatusCapsule.setBackground(BasicsGUIDefaults.BACKGROUND);
					break;
				case OPTIC:
					opticalStatusCapsule.setBackground(BasicsGUIDefaults.BACKGROUND);
					break;
				// TODO Cass - 5239
				case M3PARK:
					m3ParkStatusCapsule.setBackground(BasicsGUIDefaults.BACKGROUND);
				// TODO Cass - 5239
			}
		}
	}
}
